<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'user';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


/*PARA EL MANEJO DE USUARIOS*/

    /*
        *Esta ruta devuelve los datos de usuario al ingresar al sistema
        *@param string username El nombre de usuario que ingresa al sistema
        *@param string password El password del usuario que ingresa al sistema
        *@return array user_data
    */
    $route['loginuser']='user/login';
    /*
        *Esta ruta obtiene el catálogo de tipos de usuario disponibles en el sistema ada
        *@return array tipo_user_data
    */
    $route['tipo_user']='user/tipo_user';
    /*
        *Esta ruta obtiene los datos completos de todos los usuarios registrados en el sistema ada
        *@return array users
    */
    $route['users']='user/users';
    //Registro de usuario nuevo
        /*
            *Esta ruta permite almacenar los datos de usuario que se registra en el sistema ada
            *@param integer id identificador de usuario que proviene de base de datos moodle
            *@param status estado de la cuenta del usuario, 1 para activa
            *@param integer tipo_user_id identificador de tipo de usuario seleccionado para usuario a registrar
            *@return boolean status afirmativo en caso de que el registro sea exitoso
        */
        $route['user']='user/user';
        /*
            *Esta ruta obtiene los datos de usuario en moodle a partir de su email
            *@param string email email de usuario a consultar
            *@return array user_data datos del usuario cuyo email es el ingresado (excluye password)
        */
        $route['usermoodle/email/(:any)/domain/(:any)']='user/user_moodle/email/$1/domain/$2';
    /*
        *Ruta que permite actualizar los datos de usuario propios del sistema ada
        *@param integer id id del usuario a actualizar
        *@param status estado de la cuenta del usuario, 1 para activa
        *@param integer tipo_user_id identificador de tipo de usuario seleccionado para usuario a actualizar
        *@return boolean status afirmativo en caso de que el registro sea exitoso
    */
    $route['user']='user/user';

/*PARA LA PROGRAMACIÓN DE EVENTOS*/
    /*
        *Ruta que obtiene el listado de cursos del sistema moodle
        *@return  array cursos_data
    */
    $route['cursos']='event/cursos';
    /*
        *Ruta que obtiene el listado de unidades pertenecientes a un curso
        *@param integer courseid
        *@return  array unidades_data
    */
    $route['cursos/(:num)/unidades']='event/unidades_cursos/courseid/$1';
    /*
        *Ruta que obtiene el listado de grupos pertenecientes a un curso
        *@param integer courseid
        *@return  array grupos_data
    */
    $route['cursos/(:num)/grupos']='event/grupos_cursos/courseid/$1';
    /*
        *Ruta que obtiene el listado de actividades de determinada unidad de curso
        *@param integer unidadid
        *@return  array actividades_data
    */
    $route['unidades/(:num)/actividades']='event/actividades_unidad/unidadid/$1';

    /*
        *Ruta que obtiene el listado de actividades de determinado curso
        *@param integer cursoid
        *@return  array actividades_data
    */
    $route['cursos/(:num)/actividades']='event/actividades_curso/cursoid/$1';
    //RUTAS PARA BD ADA-MOODLE PARA PROGRAMAR EVENTOS
    /*
        *Ruta que obtiene el listado de reglas de ada
        *@return  array regla_data
    */
    $route['reglas']='event/ada_catalogo/nombre_tabla/regla';
    /*
        *Ruta que obtiene el listado de tipos de usuario de ada
        *@return  array tipo_user_data
    */
    $route['tipos_user']='event/ada_catalogo/nombre_tabla/tipo_user';
    /*
        *Ruta que obtiene el listado de uapa de ada
        *@return  array uapa_data
    */
    $route['uapas']='event/ada_catalogo/nombre_tabla/uapa';

    /*
        *Ruta que almacena los datos de de los eventos
        *@param curso_id
        *@param grupo_id
        *@param unidad_id
        *@param actividad_id
        *@param tipo_actividad_id
        *@param regla_id
        *@param mensaje
        *@param porcentaje_ponderacion
        *@param uapa_id (opcional)
        *@param user_id
        *@param fecha_inico
        *@param fecha_fin
        *@param medio_envio
        *@return boolean status
    */
    $route['evento']='event/evento';

    /*
        *Ruta que obtiene el listado de eventos de ada
        *@return  array evento_data
    */
    $route['eventos']='event/ada_catalogo/nombre_tabla/evento';

    /*
        *Ruta que obtiene el listado de eventos de ada correspondientes a una actividad moodle
        *@param actividad_id
        *@return  array evento_data
    */
//    $route['grupos/(:num)/tipo_actividad/(:num)/actividades/(:num)/eventos']='event/esventos_actividad/grupo_id/$1/tipo_actividad_id/$2/actividad_id/$3';
	$route['grupos/(:num)/tipo_actividad/(:num)/actividades/(:num)/eventos']='event/esventos_actividad/grupo_id/$1/tipo_actividad_id/$2/actividad_id/$3';
    /*
        *Ruta que obtiene el listado de eventos de ada correspondientes a un cruso moodle
        *@param curso_id
        *@return  array evento_data
    */
    $route['cursos/(:num)/grupos/(:num)/eventos']='event/eventos_cursos/curso_id/$1/grupo_id/$2';

    /*
    	*ruta que cambia estado del evento dado
        *@param evento_id
    	*@param status (estado del evento)        
    	*@return status (Estado de la operación)
    */
    $route['evento/status']='event/cambia_estado_evento/';

    $route['grupo/(:num)/vigencia'] = 'event/vigencia_grupo/grupo_id/$1';
