<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Restserver\Libraries\REST_Controller;
require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . '/libraries/Format.php';

class Event extends REST_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('moodle/ProgramacionEventosGetModel', 'mdl_prog_evento');
        $this->load->model('ada/ProgramacionEventosModel', 'ada_prog_evento');
        $this->configValidationEvent = array(
            array(
                    'field' => 'curso_id',
                    'label' => 'curso id',
                    'rules' => 'required'
            ),
            array(
                    'field' => 'grupo_id',
                    'label' => 'grupo id',
                    'rules' => 'required',
            ),
            array(
                    'field' => 'actividad_id',
                    'label' => 'actividad id',
                    'rules' => 'required'
            ),
            array(
                    'field' => 'tipo_actividad_id',
                    'label' => 'tipo actividad id',
                    'rules' => 'required'
            ),
            array(
                'field' => 'mensaje',
                'label' => 'mensaje',
                'rules' => 'required'
            ),
            array(
                'field' => 'user_id',
                'label' => 'user id',
                'rules' => 'required'
            ),
            array(
                'field' => 'fecha_fin',
                'label' => 'fecha fin',
                'rules' => 'required'
            ),
            array(
                'field' => 'medio_envio',
                'label' => 'medio envio',
                'rules' => 'required'
            ),
            array(
                'field' => 'regla_id',
                'label' => 'regla id',
                'rules' => 'required'
            )
        );
    }

    /*
        *Método que obtiene el listado de cursos del sistema moodle
        *@return  array cursos
    */
    public function cursos_get(){

        $cursos = $this->mdl_prog_evento->getCursos();

        if ($cursos){
            return $this->response(
                    array(
                        'status'    => true,
                        'message'   => 'Recuperación de catálogo de cursos exitosa',
                        'cursos_data' => $cursos
                    ),
                    200
            );
        }
        return $this->response(
                array(
                    'status'    => false,
                    'message'   => 'Catálogo de cursos no dipsonible',
                    'user_data' => null
                ),
                200
        );
    }


    /*
        *Método que obtiene el listado de unidades pertenecientes a un curso
        *@param integer courseid
        *@return  array unidades_data
    */
    public function unidades_cursos_get(){

        $unidades = $this->mdl_prog_evento->getUnidadesCurso($this->get('courseid'));

        if ($unidades){
            return $this->response(
                    array(
                        'status'    => true,
                        'message'   => 'Recuperación de unidades del curso exitosa',
                        'unidades_data' => $unidades
                    ),
                    200
            );
        }
        return $this->response(
                array(
                    'status'    => false,
                    'message'   => 'Unidades del curso no dipsonibles',
                    'unidades_data' => null
                ),
                200
        );
    }


    /*
        *Método que obtiene el listado de grupos pertenecientes a un curso
        *@param integer courseid
        *@return  array grupos
    */
    public function grupos_cursos_get(){
        $grupos = $this->mdl_prog_evento->getGruposCurso($this->get('courseid'));

        if ($grupos){
            return $this->response(
                    array(
                        'status'    => true,
                        'message'   => 'Recuperación de grupos del curso exitosa',
                        'grupos_data' => $grupos
                    ),
                    200
            );
        }
        return $this->response(
                array(
                    'status'    => false,
                    'message'   => 'Grupos del curso no dipsonibles',
                    'unidades_data' => null
                ),
                200
        );
    }

    /*
        *Método que obtiene el listado de actividades de determinado curso
        *@param integer cursoid
        *@return  array actividades_data
    */
    public function actividades_curso_get(){
        $this->actividades_curso_unidad($this->get('cursoid'));
    }


    /*
        *Método que obtiene el listado de actividades de determinada unidad de curso
        *@param integer unidadid
        *@return  array actividades_data
    */
    public function actividades_unidad_get(){
        $this->actividades_curso_unidad($this->get('unidadid'),'section');
    }

    private function actividades_curso_unidad($id,$tabla_campo_liga='course'){
        $actividades = $this->mdl_prog_evento->getActividadesCurso($id,$tabla_campo_liga);

        if ($actividades){
            return $this->response(
                    array(
                        'status'    => true,
                        'message'   => 'Recuperación de actividades exitosa',
                        'actividades_data' => $actividades
                    ),
                    200
            );
        }
        return $this->response(
                array(
                    'status'    => false,
                    'message'   => 'Actividades del curso no dipsonibles',
                    'unidades_data' => null
                ),
                200
        );
    }

    /*PARA OBTENER CATÁLOGOS DE ADA*/
    public function ada_catalogo_get(){
        $catalogo = $this->ada_prog_evento->getCatalogoAda($this->get('nombre_tabla'));

        if ($catalogo){
            return $this->response(
                    array(
                        'status'    => true,
                        'message'   => 'Recuperación de catalogo exitosa',
                        $this->get('nombre_tabla').'_data' => $catalogo
                    ),
                    200
            );
        }
        return $this->response(
                array(
                    'status'    => false,
                    'message'   => 'Catálogo de '.$this->get('nombre_tabla').' no dipsonible',
                    'user_data' => null
                ),
                200
        );
    }


    /*
        *Método que almacena los datos de de los eventos
        *@param curso_id
        *@param grupo_id
        *@param unidad_id
        *@param actividad_id
        *@param tipo_actividad_id
        *@param regla_id
        *@param mensaje
        *@param porcentaje_ponderacion
        *@param uapa_id (opcional)
        *@param user_id
        *@param fecha_inico
        *@param fecha_fin
        *@param medio_envio
        *@return boolean status
    */
    public function evento_post(){
        $this->load->library('form_validation');
        $this->form_validation->set_rules($this->configValidationEvent);
        if ($this->form_validation->run() == FALSE){
            return $this->response(
                array(
                    'status'    => false,
                    'message'   => '1<p>El evento no se guardó.</p> <p>Verifique que los datos que está almacenando sean los correspondientes a la regla que seleccionó.</p>'
                ),
                200
            );
        }
        $regla_id = $this->post('regla_id');
        $fecha_detonacion = mexfecha_mysqlfecha($this->post('fecha_fin'));
        if($regla_id == 1 || regla_id == 2 ){
            $fecha_detonacion = new DateTime($fecha_detonacion);
            $unidad_tiempo=$this->post('unidad_tiempo');
            $intervalo_prefijo =($unidad_tiempo=='H')?'PT':'P';
            $fecha_detonacion->sub(new DateInterval($intervalo_prefijo.$this->post('cantidad_tiempo').$unidad_tiempo));
            $fecha_detonacion=$fecha_detonacion->format('Y-m-d H:i:s');
        }
        var_dump($fecha_detonacion);
        $unidad_id = null;
        if ($this->post('unidad_id')!='todas'){
            $unidad_id=$this->post('unidad_id');
        }
        $respuesta = $this->ada_prog_evento->guarda_evento(
            $this->post('curso_id'),
            $this->post('grupo_id'),
            $unidad_id,
            $this->post('actividad_id'),
            $this->post('tipo_actividad_id'),
            $regla_id,
            $this->post('mensaje'),
            $this->post('porcentaje_ponderacion'),
            $this->post('uapa_id'),
            $this->post('user_id'),
            $this->post('fecha_fin'),
            $this->post('fecha_fin'),
            $this->post('medio_envio'),
            //$this->post('paginas_totales_curso'),
            $fecha_detonacion
        );
        if ($respuesta){
            return $this->response(
                    array(
                        'status'    => true,
                        'message'   => $respuesta//'El evento se guardó exitosamente.'
                    ),
                    200
            );
        }
        return $this->response(
                array(
                    'status'    => false,
                    'message'   => '<p>El evento no se guardó.</p> <p>Verifique que los datos que está almacenando sean los correspondientes a la regla que seleccionó.</p>'
                ),
                200
        );
    }

    /*
        *Método que actualiza los datos de de los eventos
        *@param evento_id
        *@param curso_id
        *@param grupo_id
        *@param unidad_id
        *@param actividad_id
        *@param tipo_actividad_id
        *@param regla_id
        *@param mensaje
        *@param porcentaje_ponderacion
        *@param uapa_id (opcional)
        *@param user_id
        *@param fecha_inico
        *@param fecha_fin
        *@param medio_envio
        *@return boolean status
    */
    public function evento_put(){
        //echo "no definida";     
        $regla_id = $this->put('regla_id');
        $fecha_detonacion = mexfecha_mysqlfecha($this->put('fecha_fin'));
        //var_dump( $this->put('regla_id'));
        if (is_null($regla_id)|| is_null($this->put('fecha_fin')) ||is_null($this->put('evento_id')) ||
        is_null($this->put('curso_id')) ||is_null($this->put('grupo_id')) ||is_null($this->put('actividad_id')) ||
        is_null($this->put('tipo_actividad_id')) || is_null($this->put('mensaje')) || is_null($this->put('user_id')) ||
        is_null($this->put('medio_envio')) ){
            return $this->response(
                array(
                    'status'    => false,
                    'message'   => '<p>El evento no se guardó.</p> <p>Revise los cambios obligatorios.</p>'
                ),
                200
            );
        }
        if($regla_id == 1 || regla_id == 2 ){
            $fecha_detonacion = new DateTime($fecha_detonacion);
            $unidad_tiempo=$this->put('unidad_tiempo');
            $intervalo_prefijo =($unidad_tiempo=='H')?'PT':'P';
            $fecha_detonacion->sub(new DateInterval($intervalo_prefijo.$this->put('cantidad_tiempo').$unidad_tiempo));
            $fecha_detonacion=$fecha_detonacion->format('Y-m-d H:i:s');
        }
        $respuesta = $this->ada_prog_evento->actualiza_evento(
            $this->put('evento_id'),
            $this->put('curso_id'),
            $this->put('grupo_id'),
            $this->put('unidad_id'),
            $this->put('actividad_id'),
            $this->put('tipo_actividad_id'),
            $regla_id,
            $this->put('mensaje'),
            $this->put('porcentaje_ponderacion'),
            $this->put('uapa_id'),
            $this->put('user_id'),
            $this->put('fecha_fin'),
            $this->put('fecha_fin'),
            $this->put('medio_envio'),
            //$this->put('paginas_totales_curso'),
            $fecha_detonacion
        );
        if ($respuesta){
            $this->ada_prog_evento->atualizaVigenciaGrupo($this->put('grupo_id'),$this->put('fecha_inicio'),$this->put('fecha_fin'), $this->put('regla_id'));
            return $this->response(
                    array(
                        'status'    => true,
                        'message'   => 'El evento se actuzlizó exitosamente.'
                    ),
                    200
            );
        }
        return $this->response(
                array(
                    'status'    => false,
                    'message'   => '<p>El evento no se actualizó.</p> <p>Verifique que los datos que está almacenando sean los correspondientes a la regla que seleccionó.</p>'
                ),
                200
        );
    }
    /*
        *Método que obtiene los eventos programados para una actividad dada
        *@param actividad_id identificador de actividad a buscar sus eventos
        *@return evetnos_data arreglo de los eventos programados para la actividad
    */
    public function esventos_actividad_get(){
        $eventos = $this->ada_prog_evento->eventos_actividad_get(
            $this->get('actividad_id'),
            $this->get('tipo_actividad_id'),
            $this->get('grupo_id')
        );

        if ($eventos){
            return $this->response(
                    array(
                        'status'    => true,
                        'message'   => 'Recuperación de eventos correspondientes a la actividad y grupos seleccionados relaizada de forma exitosa',
                        'eventos_data' => $eventos
                    ),
                    200
            );
        }
        return $this->response(
                array(
                    'status'    => false,
                    'message'   => 'No hay eventos disponibles para la actividad y grupo seleccionados',
                    'eventos_data' => null
                ),
                200
        );
    }


    private function agregaAtributoObjeto($objeto,$nombre_atributo,$valor_atributo=null,$agrega_null=false ){
        if (!$agrega_null){
            if (is_null($valor_atributo)){
                return false;
            }
        }
        $objeto_temporal = (array)$objeto;
        $objeto_temporal[$nombre_atributo]=$valor_atributo;
        return (object)$objeto_temporal;
    }

    public function eventos_cursos_get(){
        $eventos=array(
            'sin-actividad' => false,
            'con-unidad-actividad' => array()
        );
        $grupo_id = $this->get('grupo_id');
        $eventos['sin-actividad'] = $this->ada_prog_evento->eventos_cruso_sin_actividad_get(
            $this->get('curso_id'),
            $grupo_id
        );

        $unidades = $this->mdl_prog_evento->getUnidadesCurso($this->get('curso_id'));
        /*echo "<pre>UNIDAD";
        var_dump($unidades);
        echo "</pre>";*/
        if ($unidades){
            foreach($unidades as $indice => $unidad){
                
                $actividades= $this->mdl_prog_evento->getActividadesCurso($unidad->id,'section');
                if ($actividades){
                    $actividades_unidad=array();
                    foreach($actividades as $indice_actividad => $actividad){
                        //Obteniendo los eventos por actividad
                        $eventos_actividad = $this->ada_prog_evento->eventos_actividad_get(
                            $actividad->id,
                            $actividad->tipo_actividad_id,
                            $grupo_id
                        );
                        if ($eventos_actividad){
                            $actividad=$this->agregaAtributoObjeto(
                                $actividad,
                                'eventos',
                                $eventos_actividad 
                            );
                            array_push($actividades_unidad, $actividad);
                        }
                    }
                    if (count($actividades_unidad)>0){
                        $unidad = $this->agregaAtributoObjeto(
                            $unidad,
                            'actividades',
                            $actividades_unidad
                        );
                    }
                }
                
                if(property_exists ($unidad, 'actividades')){
                    array_push($eventos['con-unidad-actividad'],$unidad);
                }
            }
        }
        if (count($eventos['con-unidad-actividad'])==0){
            $eventos['con-unidad-actividad']=false;
        }

        if ($eventos['sin-actividad'] || $eventos['con-unidad-actividad']){
            return $this->response(
                    array(
                        'status'    => true,
                        'message'   => 'Recuperación de eventos correspondientes al curso xitosa',
                        'eventos_data' => $eventos
                    ),
                    200
            );
        }
        return $this->response(
                array(
                    'status'    => false,
                    'message'   => 'No hay eventos disponibles para el cuso',
                    'eventos_data' => null
                ),
                200
        );
    }


        /*
            *Método que deshabilita un evento a partir de su identificador evento
            *@param evento_id
            *@return boolean status
        */
        public function cambia_estado_evento_put(){
            $respuesta = $this->ada_prog_evento->deshabilita_evento($this->put('evento_id'),$this->put('status'));
            if ($respuesta){
                return $this->response(
                        array(
                            'status'    => true,
                            'message'   => 'Evento deshabilitado de forma exitosa'
                        ),
                        200
                );
            }
            return $this->response(
                    array(
                        'status'    => false,
                        'message'   => 'Error al deshabilitar evento, intente más tarde o contacte al administrador'
                    ),
                    200
            );
        }

    /*
        *Método que el nùmero totales de pàginas que conforman el contenido del curso cuyo id es proporcionado
        *@param integer curso_id
        *@return  paginas_totales
    */
    public function cursos_paginas_get(){
        $paginas_totales = $this->ada_prog_evento->getPaginasTotalesCurso($this->get('curso_id'));

        if ($paginas_totales){
            return $this->response(
                    array(
                        'status'    => true,
                        'message'   => 'Recuperación de número de páginas del curso exitosa',
                        'paginas_totales_data' => $paginas_totales
                    ),
                    200
            );
        }
        return $this->response(
                array(
                    'status'    => false,
                    'message'   => 'Páginas totales del curso no disponibles',
                    'paginas_totales_data' => null
                ),
                200
        );
    }

    /*
        *Método que obtiene la vigencia de un grupo
        *@param integer grupo_id
        *@return  vigencia_grupo
    */
    public function vigencia_grupo_get(){
        $vigencia_grupo = $this->ada_prog_evento->getVigenciaGrupo($this->get('grupo_id'));

        if ($vigencia_grupo){
            return $this->response(
                    array(
                        'status'    => true,
                        'message'   => 'Recuperación de vigencia del grupo exitosa',
                        'vigencia_grupo_data' => $vigencia_grupo
                    ),
                    200
            );
        }
        return $this->response(
                array(
                    'status'    => false,
                    'message'   => 'Vigencia del grupo no disponible',
                    'vigencia_grupo_data' => null
                ),
                200
        );
    }

    public function interpreta_hadita_post(){
        echo '';
        echo "evento: ". $this->post('evento_id').PHP_EOL;
        echo "alumno: ". $this->post('alumno_id').PHP_EOL;
        echo "mensaje: ". $this->post('mensaje').PHP_EOL;

        return $this->response(
            true,
            200
    );
    }
}
