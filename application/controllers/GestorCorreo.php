<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    class GestorCorreo extends CI_Controller {
        public function __construct(){
            parent::__construct();
            $this->email_origen = 'ada_bunam@cuaed.unam.mx';
            $this->usuario_origen= 'NOMBRE-CURSO - Curso de bachillerato a distancia CUAED UNAM';
            $this->password='AixeiNg4';
            $this->subject = 'Notificación de actividades';
            $this->load->library('email');
        }
        public function enviaCorreo($email_destino){
            $config = array(
                'protocol'     => 'smtp',
                'smtp_host'    => 'ssl://smtp.gmail.com',
                'smtp_port'    => '465',
                'smtp_timeout' => '7',
                'smtp_user'    => $this->email_origen,
                'smtp_pass'    => $this->password,
                'charset'      => 'utf-8',
                'newline'      => "\r\n",
                'mailtype'     => 'html',
                'validation'   => true
            );
            $this->email->clear();
            $this->email->initialize($config);
            $this->email->from($this->email_origen, $this->usuario_origen);
            $this->email->to($email_destino);
            $this->email->subject($this->subject);
            $this->email->message($this->mensaje);

            if ($this->email->send()) {
                return true;
            }else{
                return false;
            }
        }
        public  function enviaCorreoMasivo($emails_destino){
            $correos_error = array();
            foreach ($emails_destino as $clave => $email_destino){
                if (!$this->enviaCorreo($email_destino)){
                    array_push ($correos_error, $email_destino);
                }
            }
            if (count($correos_error) == 0){
                return true;
            }
            return $correos_error;
        }
    }