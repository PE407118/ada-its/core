<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Restserver\Libraries\REST_Controller;
require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . '/libraries/Format.php';

class User extends REST_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('moodle/UserModel', 'mdl_user_model');
        $this->load->model('ada/TipoUserModel', 'ada_tipo_user_model');
        $this->load->model('ada/AdaUserModel', 'ada_user_model');
    }

    /*
        *Este método devuelve los datos de usuario al ingresar al sistema
        *@param string username El nombre de usuario que ingresa al sistema
        *@param string password El password del usuario que ingresa al sistema
        *@return array user_data
    */
    public function login_post(){
        
        $password = $this->mdl_user_model->getUserPassword($this->post('email'));

        if ($password and password_verify($this->post('password'),$password->password)){
            $user = $this->mdl_user_model->getUserUsername($this->post('email'));
            if ($user){
                //Validando que el usuario se encuentre activo en el sistema Ada
                $user_ada = $this->ada_user_model->getUser($user->id);
                if ($user_ada){
                    $user=$this->agregaTipoUser($user, $user_ada);
                    return $this->response(
                            array(
                                'status'    => true,
                                'message'   => 'Ingreso al sistema exitoso',
                                'user_data' => $user
                            ),
                            200
                    );
                }

            }
        }
        return $this->response(
                array(
                    'status'    => false,
                    'message'   => 'Nombre de usuario o contraseña incorrecto',
                    'user_data' => null
                ),
                200
        );

    }

    private function agregaTipoUser($mdl_user, $ada_user){
        return (object)array_merge(
            (array)$mdl_user,
            (array)$ada_user
        );
    }

    /*
        *Este método obtiene el catálogo de tipos de usuario disponibles en el sistema ada
        *@return array tipo_user_data
    */
    public function tipo_user_get(){
        $tipo_user = $this->ada_tipo_user_model->getTiposUser();
        if ($tipo_user){
            return $this->response(
                    array(
                        'status'    => true,
                        'message'   => 'Recuperación de catálogo exitosa',
                        'tipo_user_data' => $tipo_user
                    ),
                    200
            );
        }
        return $this->response(
                array(
                    'status'    => false,
                    'message'   => 'Catálogo de tipos de usuario no dipsonible',
                    'user_data' => null
                ),
                200
        );

    }

    /*
        *Este método permite almacenar los datos de usuario que se registra en el sistema ada
        *@param integer id identificador de usuario que proviene de base de datos moodle
        *@param integer status estado de la cuenta del usuario, 1 para activa
        *@param integer tipo_user_id identificador de tipo de usuario seleccionado para usuario a registrar
        *@return boolean status afirmativo en caso de que el registro sea exitoso
    */
    public function user_post(){
        $insercion = $this->ada_user_model->setUser(
            $this->post('id'),
            $this->post('status'),
            $this->post('tipo_user_id')
        );
        if ($insercion){
            return $this->response(
                    array(
                        'status'    => true,
                        'message'   => 'Registro de usuario exitoso',
                    ),
                    200
            );
        }
        return $this->response(
                array(
                    'status'    => false,
                    'message'   => 'Registro de usuario fallido',
                    'user_data' => null
                ),
                200
        );
    }

    /*
        *Este método obtiene los datos completos de todos los usuarios registrados en el sistema ada
        *@return array users
    */
    public function users_get(){
        $user = $this->ada_user_model->getUsers();
        if ($user){
            $user_data=array();
            foreach ($user as $single_user){
                $user_moodle = $this->mdl_user_model->getUserId($single_user->id);
                if ($user_moodle){
                    $user_data[] = (object)array_merge (
                                        array("status_recovery" => true),
                                        (array)$single_user,
                                        (array)$user_moodle
                                    );
                }else{
                    $user_data[] = (object)array_merge (
                                        array("status_recovery" => false),
                                        (array)$single_user
                                    );
                }

            }
            return $this->response(
                    array(
                        'status'    => true,
                        'message'   => 'Recuperación de catálogo de usuario exitosa',
                        'user_data' => $user_data
                    ),
                    200
            );
        }
        return $this->response(
                array(
                    'status'    => false,
                    'message'   => 'Catálogo de usuarios no dipsonible',
                    'user_data' => null
                ),
                200
        );

    }

    /*
        *Este método obtiene los datos de usuario en moodle a partir de su email
        *@param string email email de usuario a consultar
        *@return array user_data datos del usuario cuyo email es el ingresado (excluye password)
    */
    public function user_moodle_get(){
        $user = $this->mdl_user_model->getUserEmail($this->get('email').'@'.$this->get('domain'));
        if ($user){
            return $this->response(
                    array(
                        'status'    => true,
                        'message'   => 'Recuperación de datos de usuario moodle exitosa',
                        'user_data' => $user
                    ),
                    200
            );
        }
        return $this->response(
                array(
                    'status'    => false,
                    'message'   => 'Datos de usuario moodle no dipsonible',
                    'user_data' => null
                ),
                200
        );

    }

    /*
        *Método que permite actualizar los datos de usuario propios del sistema ada
        *@param integer id id del usuario a actualizar
        *@param status estado de la cuenta del usuario, 1 para activa
        *@param integer tipo_user_id identificador de tipo de usuario seleccionado para usuario a actualizar
        *@return boolean status afirmativo en caso de que el registro sea exitoso
    */
    public function user_put(){
        $actualizacion = $this->ada_user_model->updateUser($this->put('id'), $this->put('status'), $this->put('tipo_user_id'));
        if ($actualizacion){
            return $this->response(
                    array(
                        'status'    => true,
                        'message'   => 'Actualización de datos de usuario exitosa',
                    ),
                    200
            );
        }
        return $this->response(
                array(
                    'status'    => false,
                    'message'   => 'No se actualizaron datos de usuario',
                    'user_data' => null
                ),
                200
        );
    }
}
