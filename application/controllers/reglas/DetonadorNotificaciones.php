<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    class DetonadorNotificaciones  extends CI_Controller{
        
        private function inicializaRegla($clave_regla,$medio_envio='Email'){
            $this->ada_eventos->clave_regla = $clave_regla;
            $this->ada_eventos->medio_envio=$medio_envio;
            $this->ada_eventos->obtenEventosVigentes();
        }

        /*
            este método funciona para obtener los datos de las notificiaciones y los alumnos a notificar 
            para las reglas de ponderación.
            Es útil tanto para el medio de envío EMAIL y ADITA
            @param clave_regla (PS para la regla de ponderación satifactoria y PNS para la regla de ponderación no satisfactoria)
            @param medio_envio (Email, ADA)
            @return eventos_notificacion (Eventos que detonan notificación)
            @return alumnos_notificar (Datos de los alumnos a notificar)
        */
        private function obtenDatosNotificaciones($clave_regla='PS',$medio_envio='Email'){
            $this->inicializaRegla($clave_regla, $medio_envio);
            $alumnos_notificar = array();
            echo "<br>Eventos recuperados<br>";
            var_dump($this->ada_eventos->eventos_recuperados);
            if ($this->ada_eventos->eventos_recuperados){
                $existen_notificaciones = false;
                foreach ($this->ada_eventos->eventos_recuperados as $clave => $evento_recuperado){
                    $exclusion = null;
                    if ($evento_recuperado->alumnos_notificados){
                        $exclusion = $evento_recuperado->alumnos_notificados;
                    }
                    echo "exclision";
                    var_dump($exclusion);
                    $alumnos_notificar[$clave] = $this->mdl_eventos->obtenDatosAlumno(
                        $evento_recuperado->actividad_id, 
                        $evento_recuperado->tipo_actividad_id, 
                        $evento_recuperado->grupo_id,
                        $this->ada_eventos->clave_regla, 
                        $evento_recuperado->porcentaje_ponderacion,
                        $exclusion,
                        $this->ada_eventos->medio_envio
                    );
                    if ($alumnos_notificar[$clave]){
                        $existen_notificaciones = true;
                    }else{
                        unset($alumnos_notificar[$clave]);
                        unset($this->ada_eventos->eventos_recuperados[$clave]);
                    }
                }
                if (count($alumnos_notificar)==0 ||  !$existen_notificaciones){
                    $alumnos_notificar = false;
                }
            }else{
                $alumnos_notificar = false;
            }
            
            echo "Alumnos a notificar";
            var_dump($alumnos_notificar );
            $datos_notificacion = array (
                'eventos_notificacion' => $this->ada_eventos->eventos_recuperados,
                'alumnos_notificar' => $alumnos_notificar 
            );
            if (!$datos_notificacion['eventos_notificacion'] || !$datos_notificacion['alumnos_notificar']){
                $datos_notificacion = false;
            }
            echo "<br>eventos y alumnos en detona:";
            var_dump($datos_notificacion);
            return $datos_notificacion;
        }
       
        private function notificaEmail($clave_regla = 'PS'){
            echo "Entra email";
            $datos_notificacion = $this->obtenDatosNotificaciones ($clave_regla, 'Email');
            if ($datos_notificacion){
                require_once( APPPATH . 'controllers/GestorCorreo.php');
                $gestor_correo = new GestorCorreo();
                foreach ($datos_notificacion['eventos_notificacion'] as $clave => $evento_notificacion){
                    $gestor_correo->mensaje = $evento_notificacion->mensaje;
                    $arreglo_alumnos = $datos_notificacion['alumnos_notificar'][$clave];
                  
                    $arreglo_email = array_column($arreglo_alumnos,'email');
                    $envio = $gestor_correo->enviaCorreoMasivo($arreglo_email);
                    if($envio){
                        foreach ($arreglo_alumnos as $clave => $alumno){
                            $this->ada_eventos->ejecutaRegistroNotificacionEnLog(
                                $alumno->id,
                                $gestor_correo->mensaje,
                                'Email',
                                $evento_notificacion->evento_id
                            );
                        }
                        return true;
                    }
                }
            }
            return false;
        }
        private function notificaAdita($clave_regla = 'PS'){
		    echo 'notificaAdita entra <br>';
            $datos_notificacion = $this->obtenDatosNotificaciones ($clave_regla, 'ADA');
            echo 'notigficaAdita regresa <br>';
            if ($datos_notificacion){
                $this->load->helper('sendhttp');
                foreach ($datos_notificacion['eventos_notificacion'] as $clave => $evento_notificacion){
                    $mensaje = $evento_notificacion->mensaje;
                    $arreglo_alumnos = $datos_notificacion['alumnos_notificar'][$clave];
                    foreach ($arreglo_alumnos as $clave => $alumno){
                        
                        echo sendPost(
                            array(
                                'alumno_id' => $alumno->id,
                                'mensaje'   => $mensaje,
                                'evento_id' => $evento_notificacion->evento_id
                            )
                        );
                        $registro_log =$this->ada_eventos->ejecutaRegistroNotificacionEnLog(
                            $alumno->id,
                            $mensaje,
                            'ADA',
                            $evento_notificacion->evento_id
                        ); 
                        $envio = $this->mdl_eventos->enviaMensajeAlumno($alumno->id,$mensaje);
                    }
                }
            }
            return false;
        }

        public function detona($clave_regla='PS'){
            $modelo_ada = '';
            $modelo_mdl = '';
            switch ($clave_regla) {
                case 'PS':
                    $modelo_ada = 'ada/reglas/PonderacionActividadAdaModel';
                    $modelo_mdl = 'moodle/reglas/PonderacionActividadModel';
                    break;
                case 'PNS':
                    $modelo_ada = 'ada/reglas/PonderacionActividadAdaModel';
                    $modelo_mdl = 'moodle/reglas/PonderacionActividadModel';
                    break;
                case 'ATG':
                    $modelo_ada = 'ada/reglas/ActividadTiempoAdaModel';
                    $modelo_mdl = 'moodle/reglas/ActividadTiempoMdlModel';
                    break;
                case 'ATE':
                    $modelo_ada = 'ada/reglas/ActividadTiempoAdaModel';
                    $modelo_mdl = 'moodle/reglas/ActividadTiempoMdlModel';
                    break;
                case 'CAS':
                    $modelo_ada = 'ada/reglas/CambioPaginaLeccionAdaModel';
                    $modelo_mdl = 'moodle/reglas/CambioPaginaLeccionMdlModel';
                            break;
                default:
                        echo "Clave de la regla no válida";
                        die();
            }
            $this->load->model($modelo_ada, 'ada_eventos');
            $this->load->model($modelo_mdl, 'mdl_eventos');

            $this->notificaAdita($clave_regla);
            $this->notificaEmail($clave_regla);

        }

	public function probar(){
		echo 'Probar <br>';
		$this->detona('CAS');
	} 

    }
