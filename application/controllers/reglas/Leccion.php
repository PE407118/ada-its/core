<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Restserver\Libraries\REST_Controller;
require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . '/libraries/Format.php';

class Leccion extends REST_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('ada/reglas/CambioPaginaLeccionAdaModel');
    }

    /*
        *Método que registra el intervalo de tiempo que un alumno vio una página de una lección 
        *@return  array cursos
    */
    public function cambioPagina_post(){
        $respuesta = $this->CambioPaginaLeccionAdaModel->registrarCambioPagina(
            $this->post('id_user'),
            $this->post('id_lesson'),
            $this->post('date'));
        var_dump($respuesta);
    }
}