<?php
    if (! defined('BASEPATH')) exit('No direct script access allowed');
    /**
     * Función que genera la fecha actual en formnato 'YYYY/MM/DD hh:mm:ss'
     * @return DATETIME Fecha de la Cd. México
     */
    function fecha_mex()
    {
        $datestring = '%Y-%m-%d %H:%i:%s';
        $tiempo     = now();
        return mdate($datestring, $tiempo);
    }

    /**
     * Función que calcula la edad del usuario
     * @param  \Datetime[] $birth Fecha de nacimiento
     * @return int         Edad calculada
     */
    function age($birth)
    {
        $year_string = '%Y';
        $tiempo     = now();
        $year_mex = mdate($year_string, $tiempo);

        $month_string = '%m';
        $tiempo     = now();
        $month_mex = mdate($month_string, $tiempo);

        $day_string = '%d';
        $tiempo     = now();
        $day_mex = mdate($day_string, $tiempo);

        $day_birth   = date("D", strtotime($birth));
        $month_birth = date("M", strtotime($birth));
        $year_birth  = date("Y", strtotime($birth));

        if (($month_birth == $month_mex) && ($day_birth > $day_mex)) {
            $year_mex = $year_mex - 1;
        }
        if ($month_birth > $month_mex) {
            $year_mex = $year_mex - 1;
        }

        $age = $year_mex - $year_birth;

        return $age;
    }

    function mexfecha_mysqlfecha($fecha){
        $formato='dd/mm/YYYY hh:ii:ss ampm';
        if ($fecha === ''){
			return FALSE;
        }
        $fecha =  preg_replace('/\//', '-', $fecha);
        //die($fecha);
        sscanf($fecha, '%d-%d-%d %s %s',$day,$month, $year, $time, $ampm);
        sscanf($time, '%d:%d:%d', $hour, $min, $sec);
        isset($sec) OR $sec = 0;
        if (isset($ampm))
		{
			$ampm = strtolower($ampm);

			if ($ampm[0] === 'p' && $hour < 12)
			{
				$hour += 12;
			}
			elseif ($ampm[0] === 'a' && $hour === 12)
			{
				$hour = 0;
			}
        }
        if ($month<10){
            $month = '0'.$month;
        }
        return $year.'-'.$month.'-'.$day.' '.$hour.':'.$min.':'.$sec;
    }
    
    function mysqlfecha_mexfecha($fecha){
        //'yyyy-mm-dd hh:ii:ss' => 'dd/mm/YYYY hh:ii:ss ampm';
        if ($fecha === ''){
			return FALSE;
        }
        //die($fecha);
        sscanf($fecha, '%d-%d-%d %s',$year,$month, $day, $time);
        sscanf($time, '%d:%d:%d', $hour, $min, $sec);
        isset($sec) OR $sec = 0;

        $ampm = 'AM';
        if ($hour >= 13){
            $ampm = 'PM';
            $hour -= 12;
        }else if ($hour == 0){
            $hour = 12;
        }

        if ($month<10){
            $month = '0'.$month;
        }
        if ($day<10){
            $day = '0'.$day;
        }
        return $day.'/'.$month.'/'.$year.' '.$hour.':'.$min.':'.$sec. ' '. $ampm;
    }