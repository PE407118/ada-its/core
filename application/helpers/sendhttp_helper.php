<?php
    function sendPost($data,$url='event/interpreta_hadita',$host=null){
        if (is_null($host)){
            $host=base_url();
        }
        $postData = '';
        //create name value pairs seperated by &
        foreach($data as $k => $v) 
        { 
          $postData .= $k . '='.$v.'&'; 
        }
        rtrim($postData, '&');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_USERPWD, 'ada-site' . ":" . 'gztuZhdBj4kaYf4q');
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_URL,$host.$url);
        curl_setopt($ch, CURLOPT_HEADER, false); 
        curl_setopt($ch, CURLOPT_POSTFIELDS,  $postData);    
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");

        $output=curl_exec($ch);

        curl_close($ch);

        return $output;
    }