<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class AdaUserModel extends CI_Model
{
    private $database;
    function __construct()
    {
        parent::__construct();
        $this->database = $this->load->database('ada', TRUE);
    }
    public function setUser( $id,  $status,  $tipo_user_id)
    {
        $this->database->set('id', $id);
        $this->database->set('status', $status);
        $this->database->set('tipo_user_id', $tipo_user_id);
        $insercion =$this->database->insert("user");
        $error = $this->database->error();

        //var_dump($error['code']);
        if ( $error['code'] != 0)
        {
                return  false; // Has keys 'code' and 'message'
        }
        return $insercion;
    }


    public function updateUser( $id,  $status,  $tipo_user_id)
    {
        $this->database->set('status', $status);
        $this->database->set('tipo_user_id', $tipo_user_id);
        $this->database->where('id', $id);
        $actualizacion =$this->database->update("user");
        if ($this->database->affected_rows()==0){
            return  false;
        }
        return $actualizacion;
    }

    public function getUsers(){
        $user = $this->db->get('user');
        if ($user and $user->num_rows()>=1){
			return $user->result();
        }
        return false;
    }

    public function getUser( $id){
        
        $this->database->where('id', $id);
        $this->database->where('status', 1);

        $user = $this->database->get('user');

        if ($user and $user->num_rows()==1){
            $user = (array)$user->result()[0];
            $this->load->model('ada/TipoUserModel', 'ada_tipo_user_model');
            $tipo_user = $this->ada_tipo_user_model->getTipoUser($user['tipo_user_id']);
            if ($tipo_user){
                return (object)array_merge($user, (array)$tipo_user);
            }

        }
        return false;
    }
}
