<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ProgramacionEventosModel extends CI_Model
{
    private $ada_database;
	function __construct()
	{
		parent::__construct();
        $this->ada_database = $this->load->database('ada', TRUE);
        $this->load->model('moodle/ProgramacionEventosGetModel', 'mdl_prog_evento');
  	}

    /*
        *Método que almacena los datos de de los eventos
        *@param curso_id
        *@param grupo_id
        *@param unidad_id
        *@param actividad_id
        *@param tipo_actividad_id
        *@param regla_id
        *@param mensaje
        *@param porcentaje_ponderacion
        *@param uapa_id (opcional)
        *@param user_id
        *@param fecha_inicio
        *@param fecha_fin
        *@param medio_envio
        *@return boolean status
    */
    public function guarda_evento($curso_id,$grupo_id,$unidad_id,$actividad_id,$tipo_actividad_id,$regla_id,$mensaje,$porcentaje_ponderacion,$uapa_id,$user_id,$fecha_inico,$fecha_fin,$medio_envio,$fecha_detonacion/*, $paginas_totales_curso*/){
        //die (to_timestamp($fecha_fin));
        /*var_dump($curso_id);
        var_dump($grupo_id);
        var_dump($unidad_id);
        var_dump($actividad_id);
        var_dump($tipo_actividad_id);
        var_dump($regla_id);
        var_dump($mensaje);
        var_dump($porcentaje_ponderacion);
        var_dump($uapa_id);
        var_dump($user_id);
        var_dump($);
        var_dump($);
        var_dump($);
        var_dump($);
        var_dump($);
        var_dump($);
        var_dump($);
        var_dump($);*/

        var_dump($fecha_detonacion);
        $this->db->start_cache();
        $this->ada_database->set('curso_id', $curso_id);
        $this->ada_database->set('grupo_id', $grupo_id);
        $this->ada_database->set('unidad_id', $unidad_id);
        $this->ada_database->set('actividad_id', $actividad_id);
        $this->ada_database->set('tipo_actividad_id', $tipo_actividad_id);
        $this->ada_database->set('regla_id', $regla_id);
        $this->ada_database->set('mensaje', $mensaje);
        $this->ada_database->set('porcentaje_ponderacion', $porcentaje_ponderacion);
        $this->ada_database->set('uapa_id', $uapa_id);
        $this->ada_database->set('user_id', $user_id);
        $this->ada_database->set('fecha_inicio', mexfecha_mysqlfecha($fecha_inico, 'dd/mm/YYY'));
        $this->ada_database->set('fecha_fin', mexfecha_mysqlfecha($fecha_fin, 'dd/mm/YYY'));
        $this->ada_database->set('medio_envio', $medio_envio);
        $this->ada_database->set('fecha_detonacion', $fecha_detonacion);
        //$this->ada_database->set('paginas_totales_curso',$paginas_totales_curso);
        
        $insercion =$this->ada_database->insert("evento");
        $error = $this->ada_database->error();
        $this->db->stop_cache();
        $this->db->flush_cache();
        //var_dump($error['code']);
        if ( $error['code'] != 0)
        {   
            //var_dump ($error);
                return  $error; // Has keys 'code' and 'message'
        }
        //$this->actualiza_paginas_totales_curso($curso_id, $paginas_totales_curso);
        return $insercion;
    }

    public function deshabilita_evento( $evento_id,$status=0){
        $this->ada_database->set('status', $status);
        $this->ada_database->where('evento_id', $evento_id);
        $actualizacion =$this->ada_database->update("evento");
        $error = $this->ada_database->error();

        if ( $error['code'] != 0)
        {
                return  false; // Has keys 'code' and 'message'
        }
        return $actualizacion;
    }


/*
    *
    *Métodos para traer los datos completos de los eventos programados
    *
*/
    //Método que selecciona todos los datos del evento, haciendo el join con las tablas correspondientes
    private function prepara_consulta_eventos_get($grupo_id){
        
        $this->ada_database->select('evento.*, uapa.nombre as uapa, regla.regla, regla.clave as clave_regla');
        $this->ada_database->from('evento');
        $this->ada_database->join('regla', 'evento.regla_id = regla.regla_id');
        $this->ada_database->join('uapa', 'evento.uapa_id = uapa.uapa_id', 'left');
        $this->ada_database->where('grupo_id', $grupo_id);
        
    }
    //metodo para generar la clave del evento
    private function obten_evento_con_clave($evento_object){
        $curso_id = $evento_object->curso_id;
        //echo "<br>curso: ".$curso_id." Evento: ".$evento_object->evento_id;
        $curso = $this->mdl_prog_evento->getCurso($curso_id);
        $evento_array = (array)$evento_object;
        if (!$curso){
            $evento_array ['clave'] = $evento_object->clave_regla.$evento_object->evento_id;
        }else{
            $evento_array ['clave'] = $curso->shortname . $evento_object->clave_regla.$evento_object->evento_id;
        }
        $evento_array ['fecha_inicio'] = mysqlfecha_mexfecha($evento_array ['fecha_inicio'] );
        $evento_array ['fecha_fin'] = mysqlfecha_mexfecha($evento_array ['fecha_fin'] );

        
        return (object)$evento_array;

    }
    private function obten_arreglo_eventos_completo($eventos_incompletos){
        $eventos_completos = array();
        foreach ($eventos_incompletos as $clave => $evento){
            $evento_con_clave=$this->obten_evento_con_clave($evento);
            array_push($eventos_completos, $evento_con_clave);
        }
        if (count ($eventos_completos)>0){
            return $eventos_completos;
        }
        return false;
    }
    //Método que devuelve los eventos ligados a una actividad
    public function eventos_actividad_get($actividad_id,$tipo_actividad_id,$grupo_id){
        $this->prepara_consulta_eventos_get($grupo_id);
        //insatnce en la tabla mdl_course_modules de  moodle
        $this->ada_database->where('actividad_id', $actividad_id);
        $this->ada_database->where('tipo_actividad_id', $tipo_actividad_id);
        $eventos = $this->ada_database->get();
        if ($eventos and $eventos->num_rows()>=1){
            return $this->obten_arreglo_eventos_completo($eventos->result());
            
        }else{
            return false;
        }
    }
    public function eventos_cruso_sin_actividad_get($curso_id,$grupo_id){
        $this->prepara_consulta_eventos_get($grupo_id);
        $this->ada_database->where('curso_id', $curso_id);
        $this->ada_database->where('actividad_id', null);
        $eventos = $this->ada_database->get();
        if ($eventos and $eventos->num_rows()>=1){
            return $this->obten_arreglo_eventos_completo($eventos->result());
        }else{
            return false;
        }
    }
    public function actualiza_paginas_totales_curso($curso_id, $paginas_totales_curso){
        $this->ada_database->set('paginas_totales_curso', $paginas_totales_curso);
        $this->ada_database->where('curso_id', $curso_id);
        $actualizacion =$this->ada_database->update("evento");
        //echo $actualizacion;
    }

    public function actualiza_evento($evento_id, $curso_id,$grupo_id,$unidad_id,$actividad_id,$tipo_actividad_id,$regla_id,$mensaje,$porcentaje_ponderacion,$uapa_id,$user_id,$fecha_inico,$fecha_fin,$medio_envio,$fecha_detonacion/*, $paginas_totales_curso*/){
        //die (to_timestamp($fecha_fin));
        $this->db->start_cache();
        $this->ada_database->set('curso_id', $curso_id);
        $this->ada_database->set('grupo_id', $grupo_id);
        $this->ada_database->set('unidad_id', $unidad_id);
        $this->ada_database->set('actividad_id', $actividad_id);
        $this->ada_database->set('tipo_actividad_id', $tipo_actividad_id);
        $this->ada_database->set('regla_id', $regla_id);
        $this->ada_database->set('mensaje', $mensaje);
        $this->ada_database->set('porcentaje_ponderacion', $porcentaje_ponderacion);
        $this->ada_database->set('uapa_id', $uapa_id);
        $this->ada_database->set('user_id', $user_id);
        $this->ada_database->set('fecha_inicio', mexfecha_mysqlfecha($fecha_inico, 'dd/mm/YYY'));
        $this->ada_database->set('fecha_fin', mexfecha_mysqlfecha($fecha_fin, 'dd/mm/YYY'));
        $this->ada_database->set('medio_envio', $medio_envio);
        $this->ada_database->set('fecha_detonacion', $fecha_detonacion);
        //$this->ada_database->set('paginas_totales_curso',$paginas_totales_curso);
        
        $this->ada_database->where('evento_id', $evento_id);
        
        $actualizacion =$this->ada_database->update("evento");
        $this->db->stop_cache();
        $this->db->flush_cache();
       // var_dump($this->ada_database->error());
        //var_dump ($actualizacion);

        if ($this->ada_database->affected_rows()==0){
            
                return  false;//$error(); // Has keys 'code' and 'message'
        }
        //$this->actualiza_paginas_totales_curso($curso_id, $paginas_totales_curso);
        return $actualizacion;
    }

    /*
		*bd ada
        *Método que obtiene el contenido de la tabla que se especifica como parámetro
		*@param $tabla_catalogo nombre de la tabla a traer
        *@return  array actividad(es)
    */
	public function getCatalogoAda($tabla_catalogo){
		$this->ada_database->where('status', 1);
		$catalogo = $this->ada_database->get($tabla_catalogo);
        if ($catalogo and $catalogo->num_rows()>=1){
 			return $catalogo->result();
        }
        return false;
    }
    
    public function getPaginasTotalesCurso($curso_id){
        $this->ada_database->distinct();
        $this->ada_database->select('paginas_totales_curso');
        $this->ada_database->where('curso_id', $curso_id);
        $this->ada_database->where('paginas_totales_curso is  NOT NULL');
        $this->ada_database->order_by('create_date', 'DESC');
        $paginas_totales = $this->ada_database->get ("evento");
        if ($paginas_totales and $paginas_totales->num_rows()>=1){
            return $paginas_totales->result()[0];
            //return $paginas_totales->result();
       }
       return false;
    }

    public function getVigenciaGrupo($grupo_id){
        $this->ada_database->distinct();
        $this->ada_database->select('fecha_inicio, fecha_fin');
        $this->ada_database->where('grupo_id', $grupo_id);
        $this->ada_database->where('fecha_inicio is  NOT NULL');
        $this->ada_database->where('fecha_fin is  NOT NULL');
        $this->ada_database->where_in('regla_id',array('1','3','4','5'));
        $vigencia = $this->ada_database->get ("evento");
        if ($vigencia and $vigencia->num_rows()==1){
            return $vigencia->result()[0];
            //return $vigencia->result();
       }
       return false;
    }

    public function atualizaVigenciaGrupo($grupo_id,$fecha_inico,$fecha_fin, $regla_id){
        if ($regla_id == 1 || $regla_id == 3 || $regla_id == 4 || $regla_id == 5){
            $this->ada_database->set('fecha_inicio', mexfecha_mysqlfecha($fecha_inico, 'dd/mm/YYY'));
            $this->ada_database->set('fecha_fin', mexfecha_mysqlfecha($fecha_fin, 'dd/mm/YYY'));
            $this->ada_database->where('grupo_id', $grupo_id);
            $this->ada_database->where_in('regla_id',array('1','3','4','5'));

            $actualizacion =$this->ada_database->update("evento");
        }
    }
}
