<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class TipoUserModel extends CI_Model
{
	function __construct()
	{
		parent::__construct();
  	}

    /*Método usado para obtener el password de un usuario a partir de su nombre de USUARIOS
        *Utilizado para login
    */
	public function getTiposUser()
	{
		$tipo_user = $this->db->get('tipo_user');
        if ($tipo_user and $tipo_user->num_rows()>=1){
			return $tipo_user->result();
        }
        return false;
	}
	public function getTipoUser( $id)
	{
		$this->db->where('tipo_user_id', $id);
		$tipo_user = $this->db->get('tipo_user');
        if ($tipo_user and $tipo_user->num_rows()==1){
			return $tipo_user->result()[0];
        }
        return false;
	}
}
