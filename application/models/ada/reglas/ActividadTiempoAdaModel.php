<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'models/ada/reglas/ReglaModel.php');
class ActividadTiempoAdaModel  extends ReglaModel
{

    public function __construct(){
        parent::__construct();
        $this->medio_envio = 'Email';
    }

    public function obtenEventosVigentes(){
        $fecha_actual = now();
        $fecha_actual_mas_min = strtotime("+1 minute", $fecha_actual);
        $this->preparaEventoGeneral('e.actividad_id,e.tipo_actividad_id,e.porcentaje_ponderacion,');
        $this->db->where('e.fecha_detonacion >=', date("Y-m-d H:i", $fecha_actual));
        $this->db->where('e.fecha_detonacion <', date("Y-m-d H:i", $fecha_actual_mas_min));
        echo "Eventos recuperados sql";
            var_dump($this->db->get_compiled_select());
        $this->obtenEventos();
    }


}