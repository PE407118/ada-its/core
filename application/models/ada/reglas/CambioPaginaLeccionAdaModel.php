<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'models/ada/reglas/ReglaModel.php');
class CambioPaginaLeccionAdaModel extends ReglaModel
{
    public function __construct(){
        parent::__construct();
        $this->medio_envio = 'Email';
        $this->load->model('moodle/reglas/CambioPaginaLeccionMdlModel'); 
    }
    public function obtenEventosVigentes(){
        echo 'obtieneEventos <br>';
        $fecha_actual = now();
        $this->preparaEventoGeneral('e.actividad_id,e.tipo_actividad_id,e.porcentaje_ponderacion');
        $this->db->where('e.fecha_fin >=', date("Y-m-d H:i", $fecha_actual));
        $this->obtenEventos();   
    }

    public function obtenEventos(){
        $eventos_recuperados_temporal = $this->db->get();
        if ($eventos_recuperados_temporal and $eventos_recuperados_temporal->num_rows()>=1){   
            $this->nombre_lecciones = $this->CambioPaginaLeccionMdlModel->getNombresLecciones($eventos_recuperados_temporal->result());
            $this->convertirArregloAsociativo();
        }
        parent::obtenEventos();
    }

    /**
     * Agrega al mensaje el nombre de la lección correspondiente al evento  
     *  */
    public function adecuaMensaje($evento){
        $mensaje= '<p>'.$evento->mensaje.'</p>'.
            '<p> Revisa de nuevo la lección <strong>'.
            $this->nombre_lecciones[$evento->actividad_id] .
            '</strong></p>';
        return $mensaje;
    }

    /**
     * Convierte el arreglo de objetos, nombre de lecciones, en un arreglo asociativo donde la llave es el id de la lección
     */
    private function convertirArregloAsociativo(){
        echo "convierte";
        //var_dump($this->nombre_lecciones);
        $arregloRelacional=array();
        foreach($this->nombre_lecciones as $leccion){
           $arregloRelacional[$leccion->id]=$leccion->name;
        }
        $this->nombre_lecciones= $arregloRelacional;
    }

     /**
      * Función que registra un campo en  lesson_timer_test con los datos correspondientes a la fecha en que accedio a esa página y la fecha en la que cambio de pagina 
      *@param id_user
      *@param id_lesson
      *@param date  (fecha en la que cambia de pagina)
      */
      public function registrarCambioPagina($id_user,$id_lesson,$date){

        $lessontime_before=$this->CambioPaginaLeccionMdlModel->ultimaConeccion($id_user,$id_lesson)->lessontime;
        
        if(($date-$lessontime_before)>60){
            
            return false;
        }

        $this->db->start_cache();
        $this->db->set('lessonid', $id_lesson);
        $this->db->set('userid', $id_user);
        $this->db->set('lessontime_before',$lessontime_before);
        $this->db->set('lessontime_after', $date);
        $this->db->set('status_notificacion', 0);
        $insercion =$this->db->insert("lesson_timer");
        $error = $this->db->error();
        $this->db->stop_cache();
        $this->db->flush_cache();
        if ( $error['code'] != 0)
        {   
                return  $error; // Has keys 'code' and 'message'
        }
        return $insercion;

     }
    
    public function getAlumnosNotificar($actividad_id){
        
        $this->db->start_cache();
        $consultaAlumnosNotificar = $this->db
            ->distinct()
            ->select ('userid')
            ->from('lesson_timer');
        echo "**************************";
        var_dump( $this->medio_envio);
        if($this->medio_envio == 'ADA'){
            $this->db->where( 'status_notificacion = 0');
        }else{
            $this->db->where( 'status_notificacion <>', $this->medio_envio);
        }
        $this->db->where("lessonid = $actividad_id");
        var_dump($this->db->get_compiled_select());
        $consultaAlumnosNotificar = $this->db->get();
        $this->db->stop_cache();
        $this->db->flush_cache();  
        if ($consultaAlumnosNotificar and $consultaAlumnosNotificar->num_rows()>=1){
            $consultaAlumnosNotificar=$consultaAlumnosNotificar->result();
            //var_dump($consultaAlumnosNotificar);
            $id_usuarios=array();
            foreach ($consultaAlumnosNotificar as $alumno) {
                $id_usuarios[]=$alumno->userid;
            }
            return implode(',',$id_usuarios);
        }else{
            var_dump($this->db->error());
            return false;
        }
    }

    public function updateStatusNotificacion($id_alumnos,$actividad_id){
        
        $this->db->start_cache();
        $this->db->set('status_notificacion', $this->medio_envio);
        $this->db->where("lessonid = ", $actividad_id);
        $this->db->where("userid in ($id_alumnos)");
        $this->db->update('lesson_timer'); 
        $this->db->stop_cache();
        $this->db->flush_cache();
        echo"<br><br>update<br><br>";
        var_dump($this->db->error());
     }
}
