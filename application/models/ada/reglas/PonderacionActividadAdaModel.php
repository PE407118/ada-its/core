<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'models/ada/reglas/ReglaModel.php');
class PonderacionActividadAdaModel extends ReglaModel
{
    public function __construct(){
        parent::__construct();
        $this->medio_envio = 'Email';
    }
    public function obtenEventosVigentes(){
        $fecha_actual = now();
        $this->preparaEventoGeneral('e.porcentaje_ponderacion,e.actividad_id,e.tipo_actividad_id,u.nombre AS nombre_uapa,u.slug as slug_uapa');
        $this->db->join ('uapa as u', 'e.uapa_id = u.uapa_id');
        $this->db->where('e.fecha_fin >=', date("Y-m-d H:i", $fecha_actual));
        $this->obtenEventos();
        
    }

    public function adecuaMensaje($evento){
        return '<p>'.$evento->mensaje.'</p>'.
            '<p><a href="'.$evento->slug_uapa.'" target="_blank" class="uapa-notificacion">'.
                $evento->nombre_uapa.
            '</a></p>';
    }

}