<?php
defined('BASEPATH') OR exit('No direct script access allowed');
abstract class ReglaModel extends CI_Model{
    function __construct(){
        parent::__construct();
        $this->db = $this->load->database('ada', TRUE);
    }

    protected function preparaEventoGeneral($campos_adicionales=''){
        $this->load->model('moodle/reglas/ReglaMdlModel');    
        $cursos_activos = $this->ReglaMdlModel->getIdCursosActivos();
        
        //$fecha_actual = now();
        //var_dump(date("Y-m-d H:i", $fecha_actual));
        //$fecha_actual_mas_min = strtotime("+1 minute", $fecha_actual);
        if ($campos_adicionales != ''){
            $campos_adicionales =',' . $campos_adicionales;
        }
        $this->db->start_cache();
        $this->db
            ->select ('e.evento_id,e.mensaje,e.grupo_id'.$campos_adicionales)
            ->from ('evento as e')
            ->join ('regla as r', 'e.regla_id = r.regla_id')
            ->where("curso_id in ($cursos_activos)")
            ->where ('r.clave', $this->clave_regla);
        //if ($this->medio_envio == 'Email'){
            //$this->db->where('e.fecha_fin >=', date("Y-m-d H:i", $fecha_actual));
            //$this->db->where('e.fecha_fin <', date("Y-m-d H:i", $fecha_actual_mas_min));
            //Reasignación forzada por la opción medio_envio = 'Ambos'
            //$medio_envio = 'Email';
        //}else{
            /*$this->db
                ->where ('e.fecha_inicio <=',date("Y-m-d H:i:s", $fecha_actual))
                ->where ('e.fecha_fin >=', date("Y-m-d H:i:s", $fecha_actual));
            $medio_envio = 'ADA';
        }*/
        $this->db   
            ->where ('e.status',1)
            ->where_in ('e.medio_envio',array($this->medio_envio,'Ambos'));
    }

    protected function obtenEventos(){
        $eventos_recuperados = $this->db->get();
        if ($eventos_recuperados and $eventos_recuperados->num_rows()>=1){
            $this->eventos_recuperados = $eventos_recuperados->result();
            $this->db->stop_cache();
            $this->db->flush_cache();
            $this->agregaCamposEnEventos();
            var_dump($this->eventos_recuperados );
        }else{
            $this->db->stop_cache();
            $this->db->flush_cache();
            $this->eventos_recuperados=false;
        }    
    }

    private function obtenLogMensajesEnviados($evento_id ){
        $this->db->start_cache();
        $this->log_enviados=$this->db
            ->where ('evento_id', $evento_id)
            ->where_in ('medio_envio', array($this->medio_envio,'Ambos'))
            ->get('mensajes_log');
        if ( $this->log_enviados and $this->log_enviados->num_rows()>=1){
            $this->log_enviados = $this->log_enviados->result();
        }else{
            $this->log_enviados=false;
        }
        $this->db->stop_cache();
        $this->db->flush_cache();
    }
    private function obtenArrayIdNotificados($evento_id){
        $this->obtenLogMensajesEnviados($evento_id);
        $ids_alumno = array();
        
        if ($this->log_enviados){
            foreach ($this->log_enviados as $clave => $log_enviado){
                array_push($ids_alumno, $log_enviado->alumno_id);
            }
        }
        if (count($ids_alumno)){
            return $ids_alumno;
        }
        return false;
    }

    private function agregaCamposEnEventos(){
        $eventos_finales = array();
        foreach ($this->eventos_recuperados as $indice => $evento_recuperado){
            $evento_temporal = (array)$evento_recuperado;
            $evento_temporal['alumnos_notificados'] = $this->obtenArrayIdNotificados($evento_temporal['evento_id']);
            $evento_temporal['mensaje']=$this->adecuaMensaje($evento_recuperado);
            array_push ($eventos_finales , (object) $evento_temporal);
        }
        $this->eventos_recuperados = $eventos_finales;
    }

    public function ejecutaRegistroNotificacionEnLog($alumno_id,$mensaje_enviado,$medio_envio,$evento_id=null){
        $this->db->start_cache();
        $this->db
            ->set('alumno_id', $alumno_id)
            ->set('mensaje_enviado',$mensaje_enviado)
            ->set('medio_envio',$medio_envio)
            ->set('evento_id', $evento_id);
        $insercion = $this->db->insert('mensajes_log');
        $error = $this->db->error();
        $this->db->stop_cache();
        $this->db->flush_cache();
        //var_dump($error);
        //var_dump($insercion); 
        if ($insercion){
            return true;
        }
        return false;
    }

    protected function adecuaMensaje($evento){
        return $evento->mensaje;
    }
}