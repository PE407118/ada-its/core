<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    class MensajeroMdlModel extends CI_Model{
        public function __construct(){
            parent::__construct();
            $this->db = $this->load->database('moodle', TRUE);
        }
    
        public function enviaMensajeAlumno($alumno_id,$mensaje){
            $adita_id = $this->obtenIdAdita();
        
        
            if ($adita_id){
                $conversacion_id = $this->obtenConversacionAdaAlumno($adita_id,$alumno_id);
                if (!$conversacion_id){
                
                    return false;
                }
            
            
                $envio = $this->enviaMensaje($adita_id,$conversacion_id,$mensaje);
                if ($envio){
                    return true;
                }
            }
            return false;
        }

        private function enviaMensaje($adita_id,$conversacion_id,$mensaje){
            $this->db->start_cache();
            $envio = $this->db
                ->set ('useridfrom', $adita_id)
                ->set ('conversationid', $conversacion_id)
                ->set ('fullmessage', $mensaje)
                ->set('fullmessageformat',0)
                ->set('smallmessage',$mensaje)
                ->set('timecreated',now())
                ->insert('messages');
            $error = $this->db->error();
            $this->db->stop_cache();
            $this->db->flush_cache();
        
            if ( $error['code'] != 0)
            {
                return  false; // Has keys 'code' and 'message'
            }
            //$this->actualiza_paginas_totales_curso($curso_id, $paginas_totales_curso);
            return $envio;
        }
        private function obtenIdAdita(){
            $this->db->start_cache();
            $adita = $this->db
                ->select('id')
                ->where('username', 'hadita')
                ->get('user');
            if ($adita and $adita->num_rows()==1){
                $adita= $adita->result()[0]->id;
            }else{
                $adita=false;
            }
            $this->db->stop_cache();
            $this->db->flush_cache();
            return $adita;
        }
        private function obtenConversacionAdaAlumno($adita_id,$alumno_id){
            $conversacion_id = $this->buscaConversacionAdaAlumno($adita_id,$alumno_id);
            if(!$conversacion_id){
                $conversacion_id = $this->iniciaConversacion($adita_id,$alumno_id);
                if(!$conversacion_id){
                    return false;
                }
            }
            return $conversacion_id;
        }
        private function buscaConversacionAdaAlumno($adita_id,$alumno_id){
            $this->db->start_cache();
            $conversacion_id = $this->db
                ->select('conversationid')
                ->where_in('userid', array($adita_id,$alumno_id))
                ->group_by('conversationid')
                ->having('count(*)',2)
                ->get('message_conversation_members');
            if ($conversacion_id and $conversacion_id->num_rows()==1){
                $conversacion_id= $conversacion_id->result()[0]->conversationid;
            }else{
                $conversacion_id=false;
            }
            $this->db->stop_cache();
            $this->db->flush_cache();
            return $conversacion_id;
        }

        private function iniciaConversacion($adita_id,$alumno_id){
            $this->db->trans_start();
            $conversacion_id = $this->registraConversacion();
            if (!$conversacion_id){
            
                return false;
            }
        
            $registro_adita_conversacion = $this->registraMiembroConversacion($conversacion_id,$adita_id);
            if (!$registro_adita_conversacion){

                return false;
            }
            $registro_alumno_conversacion = $this->registraMiembroConversacion($conversacion_id,$alumno_id);
            if (!$registro_alumno_conversacion){
                return false;
            }
            $this->db->trans_complete();
            return $conversacion_id;
        }
        private function registraConversacion(){
            $this->db->start_cache();
            $conversacion_id = false;
            $registro_conversacion = $this->db
                ->set('type',1)
                ->set('enabled',1)
                ->set('timecreated', now())
                ->insert('message_conversations');
            if ($registro_conversacion){
                $conversacion_id = $this->db->insert_id();
            }
            $this->db->stop_cache();
            $this->db->flush_cache();
            return $conversacion_id;
        }

        private function registraMiembroConversacion($conversacion_id,$miembro_id){
            $this->db->start_cache();
            $registro = false;
            $registro_conversacion = $this->db
                ->set('conversationid',$conversacion_id)
                ->set('userid',$miembro_id)
                ->set('timecreated', now())
                ->insert('message_conversation_members');
            if ($registro_conversacion){
                $registro=true;
            }
            $this->db->stop_cache();
            $this->db->flush_cache();
            return $registro;
        }
    }