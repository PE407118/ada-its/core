<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ProgramacionEventosGetModel extends CI_Model
{
	private $mdl_database;
    private $ada_database;
	function __construct()
	{
		parent::__construct();
		$this->mdl_database = $this->load->database('moodle', TRUE);
        $this->ada_database = $this->load->database('ada', TRUE);
  	}

    /*
		*bd moodle
        *Método usado para obtener un arreglo con el listado de todos los cursos disponibles en moodle
        *Utilizado para programación de eventos
    */
	public function getCursos()
	{
		$this->mdl_database->select("id,fullname,shortname,FROM_UNIXTIME(startdate, '%d-%m-%Y %h:%i:%s') as startdate,FROM_UNIXTIME(enddate, '%d-%m-%Y %h:%i:%s') as enddate");
		$courses = $this->mdl_database->get('course');
        if ($courses and $courses->num_rows()>=1){
			return $courses->result();
        }
        return false;
	}
	/*
		*bd moodle
		*Método usado para obtener los datos del curso cuyo id es proporcionado
		*@param curso_id
		*@return curso_data
    */
	public function getCurso($curso_id)
	{
		$this->mdl_database->select("id,fullname,shortname,FROM_UNIXTIME(startdate, '%d-%m-%Y %h:%i:%s') as startdate,FROM_UNIXTIME(enddate, '%d-%m-%Y %h:%i:%s') as enddate");
		$this->mdl_database->where('id', $curso_id);
		$courses = $this->mdl_database->get('course');
        if ($courses and $courses->num_rows()==1){
			return $courses->result()[0];
        }
        return false;
	}

    /*
		*bd moodle
        *Método que obtiene el listado de unidades pertenecientes a un curso
        *@param integer courseid
        *@return  array unidades_data
    */
	public function getUnidadesCurso($cursoid)
	{
		$this->mdl_database->select('id,name,sequence');
        $this->mdl_database->where('course', $cursoid);
		$unidades = $this->mdl_database->get('course_sections');
        if ($unidades and $unidades->num_rows()>=1){
			return $unidades->result();
        }
        return false;
	}

    /*
		*bd moodle
        *Método que obtiene el listado de grupos pertenecientes a un curso
        *@param integer courseid
        *@return  array grupos
    */
    public function getGruposCurso($courseid){
        $this->mdl_database->select('id,name');
        $this->mdl_database->where('courseid', $courseid);
		$grupos = $this->mdl_database->get('groups');
        if ($grupos and $grupos->num_rows()>=1){
			return $grupos->result();
        }
        return false;
    }

	/*
		*bd moodle
        *Método que obtiene el listado de actividades de determinado curso
		*Si se indica la tabla de unidades, hace el filtrado de actividades por unidad del curso seleccionada
        *@param integer id identificador de la unidad/curso
        *@return  array actividades
    */
	public function getActividadesCurso($id,$tabla_campo_liga='course'){//Si se requiere por unidad, enviar $tabla_campo_liga='section'
		//Obteniendo registros de la tabla "transitivia"
		$course_modules = $this->getCourseModules($id, $tabla_campo_liga);
		if($course_modules){
			$actividades = array();
			foreach ($course_modules as $indice => $single_course_module){
				//Se obtiene el tipo de actividad para saber en qué tabla buscar los datos de la actividad
				$tipo_actividad = $this->getModules($single_course_module->module);
				$actividad=false;
				if ($tipo_actividad){
					//Se buscan los datos de la actividad
					$actividad = (array)$this->getActividadesData($tipo_actividad->name,$single_course_module->instance);
					$actividad['tipo_actividad_id']=$tipo_actividad->id;
					$actividad=(object)$actividad;
				}
				if ($actividad){
					array_push($actividades, $actividad);
				}
			}
			if (count($actividades)>0){
				return $actividades;
			}
		}
		return false;

	}


	/*
		*bd moodle
        *Método que obtiene los registros de la tabla "transitiva" entre el curso/unidad y las actividades
        *@param integer id
        *@return  array course_modules
    */
	private function getCourseModules($id, $tabla_campo_liga='course'){
		$this->mdl_database->select('id,section,course,module,instance');
		$this->mdl_database->where($tabla_campo_liga, $id);
		$course_modules = $this->mdl_database->get('course_modules');
		if ($course_modules and $course_modules->num_rows()>=1){
			return $course_modules->result();
        }
        return false;
	}

	/*
		*bd moodle
        *Método que obtiene el listado de módulos (tipos de actividad) de moodel
		*Si se indica la tabla de unidades, hace el filtrado de actividades por unidad del curso seleccionada
		*Si se indica el id, se obtienen un único registro del tipo de actividad
        *@return  array tipo(s)_actividad
    */
	public function getModules($id=null){
		$this->db->stop_cache();
		$this->db->start_cache();
		$this->mdl_database->select('id,name');

		if (!is_null($id)){
			$this->mdl_database->where('id',  (int)$id);
		}
		$tipos_actividad = $this->mdl_database->get('modules');
		//return $tipos_actividad;

		if ($tipos_actividad and $tipos_actividad->num_rows()>=1){
			$tipos_actividad=$tipos_actividad->result();
			if (!is_null($id)){
				$tipos_actividad=$tipos_actividad[0];
			}
			return $tipos_actividad;
        }
        return false;
	}

	/*
		*bd moodle
        *Método que obtiene el listado de actividades de un tipo de actividad
		*Si se indica el id, se obtienen un único registro de la actividad de la tabla indicada
        *@return  array actividad(es)
    */
	public function getActividadesData($actividad_tabla='assign',$id=null){
		$this->db->stop_cache();
		$this->db->start_cache();
		$this->mdl_database->select('id ,name');

		if (!is_null($id)){
			$this->mdl_database->where('id', $id);
		}
		$actividad = $this->mdl_database->get($actividad_tabla);
		if ($actividad and $actividad->num_rows()>=1){
			$actividad=$actividad->result();
			if (!is_null($id)){
				$actividad=$actividad[0];
			}
			return $actividad;
        }
        return false;
	}

	
}
