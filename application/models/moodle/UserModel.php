<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class UserModel extends CI_Model
{
	private $database;
	function __construct()
	{
		parent::__construct();
		$this->database = $this->load->database('moodle', TRUE);
  	}

    /*Método usado para obtener el password de un usuario a partir de su nombre de USUARIOS
        *Utilizado para login
    */
	public function getUserPassword( $email)
	{
		$this->database->select('password');
		$this->database->where('email', $email);
		$password = $this->database->get('user');
        if ($password and $password->num_rows()==1){
			return $password->result()[0];
        }
        return false;
	}
    public function getUserUsername( $email)
	{
		$this->database->where('email', $email);
		return $this->getUser();
	}
	public function getUserId( $id)
	{
		$this->database->where('id', $id);
		return $this->getUser();
	}
	public function getUserEmail( $email)
	{
		$this->database->where('email', $email);
		return $this->getUser();
	}

	private function getUser(){
		$this->database->select('id, username, firstname, lastname, email');
		$user = $this->database->get('user');
        if ($user and $user->num_rows()==1){
			return $user->result()[0];
        }
        return false;
	}
}
