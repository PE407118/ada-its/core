<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'models/moodle/reglas/ReglaMdlModel.php');
class ActividadTiempoMdlModel extends ReglaMdlModel{
    
    public function obtenDatosAlumno($actividad_id, $tipo_actividad_id, $grupo_id,$clave_regla, $porcentaje_ponderacion,$exclusion= null,$medio_envio){
        $caso_subconsulta = $this->obtenTablaTipoActividad($tipo_actividad_id);
        $subconsulta = $this->obtenSubconsulta($caso_subconsulta,$actividad_id);
        if ($subconsulta){
            $this->preparaAlumnoGeneral($grupo_id,$exclusion);
            $this->db->where("u.id not in ($subconsulta)", NULL, FALSE);
            $this->ejecutaRecuperacionDatosAlumno();
            return $this->datos_alumno_recuperados;
        }
        return false;
    }

    private function obtenSubconsulta($tipo_actividad,$actividad_id){
        $this->db->start_cache();
        $subconsulta=false;
        switch ($tipo_actividad) {
            case 'quiz':
                $subconsulta=$this->preparaSubconsultaQuiz($actividad_id);
                break;
            case 'assign':
                $subconsulta=$this->preparaSubconsultaAssign($actividad_id);
                break;
            case 'forum':
                $subconsulta=$this->preparaSubconsultaForum($actividad_id);
                break;
            default:
                $subconsulta=false;
                break;
        }
        $this->db->stop_cache();
        $this->db->flush_cache();  
        return $subconsulta;
    }

    private function preparaSubconsultaQuiz($actividad_id){
        $subconsulta = $this->db
            ->distinct()
            ->select ('userid')
            ->from('quiz_attempts')
            ->where('state','finished')
            ->where('quiz',$actividad_id)
            ->get_compiled_select();
        return $subconsulta;
    }
    private function preparaSubconsultaAssign($actividad_id){
        $subconsulta = $this->db
            ->distinct()
            ->select ('userid')
            ->from('assign_submission')
            ->where('status','submitted')
            ->where('ASSIGNMENT',$actividad_id)
            ->get_compiled_select();
        return $subconsulta;
    }
    private function preparaSubconsultaForum($actividad_id){
        $subconsulta = $this->db
            ->distinct()
            ->select ('userid')
            ->from('forum_posts as fp')
            ->join('forum_discussions as fd','fp.discussion = fd.id')
            ->where('forum',$actividad_id)
            ->get_compiled_select();
        return $subconsulta;
    }
    private function obtenTablaTipoActividad($tipo_actividad_id){
        $this->db->start_cache();
        $tipo_actividad =  $this->db
                ->select('name')
                ->where('id', $tipo_actividad_id)
                ->get('modules');
        if($tipo_actividad and $tipo_actividad->num_rows()==1){
            $tipo_actividad = $tipo_actividad->result()[0]->name;
        }else{
            $tipo_actividad = false;
        }
        $this->db->stop_cache();
        $this->db->flush_cache();  
        return $tipo_actividad;

    }
}