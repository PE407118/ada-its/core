<?php
require(APPPATH.'models/moodle/reglas/ReglaMdlModel.php');
class CambioPaginaLeccionMdlModel extends ReglaMdlModel{

    public function obtenDatosAlumno($actividad_id, $tipo_actividad_id, $grupo_id,$clave_regla, $porcentaje_ponderacion,$exclusion= null,$medio_envio){
        $this->load->model('moodle/reglas/CambioPaginaLeccionAdaModel'); 
        $this->CambioPaginaLeccionAdaModel->medio_envio =$medio_envio;
        $consultaAlumnosNotificar = $this->CambioPaginaLeccionAdaModel->getAlumnosNotificar($actividad_id);/*$this->db
            ->distinct()
            ->select ('userid','lessontime_before' , 'lessontime_after','lessonid','status_notificacion')
            ->from('lesson_timer_test')
            ->where( 'status_notificacion = 0')
            ->where("lessonid = $actividad_id")
            ->get_compiled_select();
        $this->db->stop_cache();
        $this->db->flush_cache();  */
        echo '<br>alumnos notificar:<br>';
        var_dump($consultaAlumnosNotificar);
        echo '<br>grupo id:<br>';
        //var_dump($grupo_id);
        
        if ($consultaAlumnosNotificar){
            $this->preparaAlumnoGeneral($grupo_id,null);
            echo '<br>alumnos 1:<br>';
           // var_dump($this->db->last_query());
            $this->db->where("u.id in ($consultaAlumnosNotificar)");
            echo '<br>alumnos 2:<br>';
            //var_dump($this->db->last_query());
            $this->ejecutaRecuperacionDatosAlumno();
            $this->actualizarStatusNotificacion($actividad_id);
            return $this->datos_alumno_recuperados;
        }
        return false;
    }

    /**
     * Se obtiene los nombres de las lecciones correspondientes a cada evento de una lista de eventos
     * @eventos
     */
     public function getNombresLecciones($eventos){

        if(!$eventos){
            echo 'if get nombres<br>';
             return false;
        }
        echo 'get nombres<br>';
        $id_lecciones = array();
        foreach($eventos as $evento){
        	$id_lecciones[] = $evento->actividad_id;
        }
        echo 'lecciones id: ';
        //var_dump($id_lecciones);
        $id_lecciones= implode(",",$id_lecciones);
        $this->db->start_cache();
        $this->db
            ->distinct()
            ->select ('id, name')
            ->from('lesson')
            ->where( "id in ($id_lecciones)", NULL, FALSE);
        $nombres_lecciones= $this->db->get();
        echo 'lecciones nombres: ';
        //var_dump($nombres_lecciones->result());
        $this->db->stop_cache();
        $this->db->flush_cache();  
        return $nombres_lecciones->result();
     }

    
     
     /**
      * Función para obtener la ultima fecha en la que accedio un usuario a una lección 
      *@param id_usuario
      *@param id_lesson 
      */
     public function ultimaConeccion($id_user,$id_lesson){
        $this->db->start_cache();
        $ultima_coneccion = $this->db
            ->distinct()
            ->select ('lessontime,lessonid,userid')
            ->from('lesson_timer')
            ->where( "lessonid = ",$id_lesson)
            ->where("userid = ",$id_user);
        $this->db->order_by("lessontime", "desc");
        echo "get<br>";
        //var_dump($ultima_coneccion->get_compiled_select());
        $ultima_coneccion = $this->db->get();
        $this->db->stop_cache();
        $this->db->flush_cache();
        return $ultima_coneccion->row();
     }

    private function actualizarStatusNotificacion($actividad_id){
        if(!$this->datos_alumno_recuperados){
            return false;
        }
        $id_alumnos=array();
       foreach( $this->datos_alumno_recuperados as $alumno){
            array_push($id_alumnos, $alumno->id);
       }
       $id_alumnos= implode( ',',$id_alumnos);
       $this->CambioPaginaLeccionAdaModel->updateStatusNotificacion($id_alumnos,$actividad_id);
     }

}
