<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'models/moodle/reglas/ReglaMdlModel.php');
class PonderacionActividadModel extends ReglaMdlModel
{
    /*
        *Método que realiza una consulta a la base de datos de moodle versión 3.6
        *obtiene la evaluación que tienen todos los alumnos pertenecientes a un grupo específico de una determinada actividad
        *@param actividad_id (conocido como item o instance en moodle)
        *@param tipo_actividad_id (conocido como módulo en moodle)
        *@param grupo_id (grupo a monitorear)
    */
    public function obtenDatosAlumno($actividad_id, $tipo_actividad_id, $grupo_id,$clave_regla, $porcentaje_ponderacion,$exclusion= null,$medio_envio){
        $calificacion_comparativa = $this->obtenComparacionFinalgrade($actividad_id, $tipo_actividad_id, $grupo_id,$porcentaje_ponderacion);
        if ($calificacion_comparativa){
            $comparacion_calificacion = '<';
            if ($clave_regla == 'PS'){
                $comparacion_calificacion = '>=';
            }
            $this->preparaAlumnoGeneral($grupo_id,$exclusion,$campos_adicionales='gi.itemname AS nombre_actividad, gg.finalgrade,gg.rawgrademin,gg.rawgrademax');
            echo "regresa hijo";
            $this->db
                    ->join('grade_grades as gg', 'u.id = gg.userid')
                    ->join('grade_items as gi', 'gi.id=gg.itemid')
                    ->join('modules as m', 'gi.itemmodule = m.name')
                    ->join('course_modules as cm', 'm.id = cm.module and gi.iteminstance = cm.`instance`')
                ->where('gi.itemname is not null')
                ->where('gi.itemname !=', '')
                ->where('m.id', $tipo_actividad_id)
                ->where('cm.`instance`',$actividad_id)
                ->where('gg.finalgrade'.$comparacion_calificacion,$calificacion_comparativa);
            $this->ejecutaRecuperacionDatosAlumno();
            
            return $this->datos_alumno_recuperados;
        }
        return false;
    }
    private function obtenComparacionFinalgrade($actividad_id, $tipo_actividad_id, $grupo_id,$porcentaje_ponderacion){
        $this->db->start_cache();
        $calificacion_comparativa = $this->db
            ->distinct()
            ->select('gg.rawgrademax')
            ->from('user as u')
                ->join('role_assignments as ra', 'ra.userid = u.id')
                ->join('role as r', 'ra.roleid = r.id')
                ->join('context as con', 'ra.contextid = con.id')
                ->join('course as c', 'c.id = con.instanceid')
                ->join('groups_members as gm', 'u.id=gm.userid')
                ->join('groups as g', 'g.id = gm.groupid')
                ->join('grade_grades as gg', 'u.id = gg.userid')
                ->join('grade_items as gi', 'gi.id=gg.itemid')
                ->join('modules as m', 'gi.itemmodule = m.name')
                ->join('course_modules as cm', 'm.id = cm.module and gi.iteminstance = cm.`instance`')
            ->where ('con.contextlevel',50)
            ->where('gi.itemname is not null')
            ->where('gi.itemname !=', '')
            ->where('m.id', $tipo_actividad_id)
            ->where('cm.`instance`',$actividad_id)
            ->where('r.shortname','student')
            ->where('g.id', $grupo_id)
            //->where_not_in('u.id', $exclusion)
            ->get ();
        if ($calificacion_comparativa and $calificacion_comparativa->num_rows()==1){
            $calificacion_comparativa = $calificacion_comparativa->result()[0]->rawgrademax*$porcentaje_ponderacion/100;

        }else{
            $calificacion_comparativa=false;
        }
        $this->db->stop_cache();
        $this->db->flush_cache();
        return $calificacion_comparativa;
    }
}