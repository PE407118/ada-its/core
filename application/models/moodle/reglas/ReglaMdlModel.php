<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    require(APPPATH.'models/moodle/MensajeroMdlModel.php');
    class ReglaMdlModel extends MensajeroMdlModel{
        protected function preparaAlumnoGeneral($grupo_id,$exclusion=null,$campos_adicionales=''){
            
            if ($campos_adicionales != ''){
                $campos_adicionales =',' . $campos_adicionales;
            }
            $this->db->start_cache();
            $this->db
                ->select ('u.id, u.username, u.firstname, u.lastname, u.email'.$campos_adicionales)
                ->from('user as u')
                    ->join('role_assignments as ra', 'ra.userid = u.id')
                    ->join('role as r', 'ra.roleid = r.id')
                    ->join('context as con', 'ra.contextid = con.id')
                    ->join('course as c', 'c.id = con.instanceid')
                    ->join('groups_members as gm', 'u.id=gm.userid')
                    ->join('groups as g', 'g.id = gm.groupid')
                ->where ('con.contextlevel',50)
                ->where('r.shortname','student')
                ->where('g.id', $grupo_id);
            if (!is_null($exclusion)){
                $this->db->where_not_in('u.id', $exclusion);
            }
            echo "<br>termina prepara alumno<br>";
        }

        protected function ejecutaRecuperacionDatosAlumno(){
            echo"<br><br>Alumnos: <br>";
            var_dump($this->db->get_compiled_select());
            $datos_alumno = $this->db->get();
            if ($datos_alumno and $datos_alumno->num_rows()>=1){
                $this->datos_alumno_recuperados = $datos_alumno->result();
            }else{
                $this->datos_alumno_recuperados=false;
            }  
            $this->db->stop_cache();
            $this->db->flush_cache();  
        }
        
        public function getIdCursosActivos(){
            $fecha_actual=now();
            $this->db->start_cache();
            $cursos_activos =$this->db
                ->select ('id,startdate')
                ->from('course')
                ->where('startdate <=', now())
                ->get();
            if($cursos_activos and $cursos_activos->num_rows()>=1){
                $cursos_activos= $cursos_activos->result();
                $id_cursos_activos = '';
                foreach($cursos_activos as $curso){
                    if($id_cursos_activos == ''){
                        $id_cursos_activos =$curso->id;
                    }else{ 
                        $id_cursos_activos .=','.$curso->id;
                    }
                }
                $this->db->stop_cache();
                $this->db->flush_cache();  
                echo "<br>Entra cursos activos<br>";
                return $id_cursos_activos;
            }
            $this->db->stop_cache();
            $this->db->flush_cache();  
            return false;
        }
    }