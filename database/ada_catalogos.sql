insert into ada.regla (regla_id,clave,regla,mensaje) values
	(1,'ATG','Actividades en tiempo de la vigencia del grupo','¡Entrega a tiempo tu actividad! Recuerda que la actividad no se ha realizado y el curso está por concluir.'),
	(2,'ATE','Actividad correspondiente en tiempo específico','¡Entrega a tiempo tu actividad! Recuerda que la actividad tiene una fecha y hora específica de entrega.'),
	(3,'CAS','Cambiar de página con actividad sin resolver en menos de 60 segundo.','Estimado alumno, le sugerimos revisar el contenido de la siguiente UAPA para complementar sus conocimientos sobre esta habilidad'),
	(4,'PS','Ponderación satisfactoria','¡Felicidades lo haz hecho muy bien! Te sugerimos el siguiente material para continuar tu aprendizaje.'),
	(5,'PNS','Ponderación no satisfactoria','¡Puedes hacerlo mejor! Te sugerimos el siguiente material para reforzar tu aprendizaje.'),
	(6,'PAC','Porcentaje de avance del curso','¡Vamos, tu puedes hacerlo! El curso está por concluir, revisa el porcentaje de avance que tienes en el curso.'),
	(7,'VCT','Visualización del contenido en un tiempo específico','¡El tiempo sigue avanzando! Procura ir avanzando en el contenido y actividades en el tiempo sugerido.');
	
insert into ada.tipo_user(tipo_user_id,tipo_user) values 
	(1,'Administrador'),
	(2,'Coordinador'),
	(3,'Tutor');
	
insert into ada.uapa(uapa_id,nombre,slug) values
	(1,'uapa1','u1'),
	(2,'uapa2','u2'),
	(3,'uapa3','u3');

ALTER TABLE ada.regla AUTO_INCREMENT = 8;
ALTER TABLE ada.tipo_user AUTO_INCREMENT = 3;
ALTER TABLE ada.uapa AUTO_INCREMENT =4;