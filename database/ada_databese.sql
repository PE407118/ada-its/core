-- MySQL Workbench Forward Engineering


-- -----------------------------------------------------
-- Schema ada
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `ada` DEFAULT CHARACTER SET utf8 ;
USE `ada` ;

-- -----------------------------------------------------
-- Table `ada`.`tipo_user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ada`.`tipo_user` (
  `tipo_user_id` INT NOT NULL AUTO_INCREMENT,
  `tipo_user` VARCHAR(100) NOT NULL COMMENT 'Recibe valores de \n- Administrador\n- Coordinador\n- Tutor',
  `status` INT NOT NULL DEFAULT 1,
  `create_date` TIMESTAMP NOT NULL,
  `update_date` TIMESTAMP NULL,
  PRIMARY KEY (`tipo_user_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ada`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ada`.`user` (
  `id` BIGINT(10) NOT NULL,
  `status` INT NOT NULL DEFAULT 1,
  `create_date` TIMESTAMP NOT NULL,
  `update_date` TIMESTAMP NULL,
  `tipo_user_id` BIGINT(10) NOT NULL REFERENCES `ada`.`tipo_user` (`tipo_user_id`),
  PRIMARY KEY (`id`)
)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ada`.`regla`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ada`.`regla` (
  `regla_id` INT NOT NULL AUTO_INCREMENT,
  `regla` VARCHAR(175) NOT NULL,
  `mensaje` LONGTEXT NULL,
  `status` INT NOT NULL DEFAULT 1,
  `craete_date` TIMESTAMP NOT NULL,
  `update_date` TIMESTAMP NULL,
  `clave` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`regla_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ada`.`uapa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ada`.`uapa` (
  `uapa_id` BIGINT(10) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(255) NOT NULL,
  `slug` VARCHAR(255) NOT NULL,
  `status` INT NOT NULL DEFAULT 1,
  `create_date` TIMESTAMP NOT NULL,
  `update_date` TIMESTAMP NULL,
  PRIMARY KEY (`uapa_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ada`.`evento`
-- -----------------------------------------------------
CREATE TABLE `evento` (
  `evento_id` bigint(10) NOT NULL AUTO_INCREMENT,
  `mensaje` longtext NOT NULL,
  `regla_id` int(11) NOT NULL,
  `uapa_id` bigint(10) DEFAULT NULL,
  `user_id` bigint(10) NOT NULL COMMENT 'Usuario que hizo la creación/última modificación del evento',
  `status` int(11) NOT NULL DEFAULT '1',
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_Date` timestamp NULL DEFAULT NULL,
  `grupo_id` bigint(10) NOT NULL,
  `actividad_id` bigint(10) DEFAULT NULL COMMENT 'En la base de moodle, este id es el campo id de la tabla correspondiente al name del tipo _actividad_id (module name en moodle)',
  `tipo_actividad_id` bigint(10) DEFAULT NULL COMMENT 'En la base module la tabla se conoce como module',
  `fecha_inicio` timestamp NULL DEFAULT NULL,
  `fecha_fin` timestamp NULL DEFAULT NULL,
  `medio_envio` varchar(45) NOT NULL COMMENT 'Toma el valor de\nAda\nCorreo electrónico\nAmbas\n',
  `porcentaje_ponderacion` float DEFAULT NULL COMMENT 'para la reglas reglas de evaluación con porcentaje satisfactorio/no satisfactorio',
  `curso_id` int(11) DEFAULT NULL,
  `unidad_id` int(11) DEFAULT NULL,
  `paginas_totales_curso` int(11) DEFAULT NULL,
  PRIMARY KEY (`evento_id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;


-- -----------------------------------------------------
-- Table `ada`.`alumno_evento`
-- -----------------------------------------------------
CREATE TABLE `mensajes_log` (
  `mensajes_log_id` int(11) NOT NULL AUTO_INCREMENT,
  `alumno_id` varchar(45) NOT NULL,
  `mensaje_enviado` longtext NOT NULL,
  `medio_envio` varchar(45) NOT NULL,
  `evento_id` bigint(10) DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`mensajes_log_id`),
  KEY `evento_id` (`evento_id`),
  CONSTRAINT `mensajes_log_ibfk_1` FOREIGN KEY (`evento_id`) REFERENCES `evento` (`evento_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;


--alter table evento add column paginas_totales_curso int;