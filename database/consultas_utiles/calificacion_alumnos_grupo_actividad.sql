
##APARTADO PARA TRATAR DE OBTENER LAS CALIFICACONES DE LOS ALUMNOS DE DETERMINADO (GRUPO, ACTIVIDAD)

#obtenidendo los usuarios pertenecientes a un grupo especìfico (el grupo solo tiene un curso)
SELECT u.id, u.username FROM
mdl_user u
JOIN mdl_role_assignments ra ON ra.userid = u.id
JOIN mdl_role r ON ra.roleid = r.id
JOIN mdl_context con ON ra.contextid = con.id
JOIN mdl_course c ON c.id = con.instanceid AND con.contextlevel = 50
JOIN mdl_groups_members gm on u.id=gm.userid 
join mdl_groups g on g.id = gm.groupid
WHERE
r.shortname = 'student' 
AND g.id = 1;


#obtenidendo las calificaciones de un usuario en un curso
SELECT gi.id,gg.finalgrade,gg.rawgrademin,gg.rawgrademax
FROM mdl_grade_items gi LEFT JOIN mdl_grade_grades gg 
	ON gi.id=gg.itemid AND gg.userid=11
    WHERE gi.courseid = 4;
   
 #Obteniendo gi con el criterio de bùsqueda de actividad
 select gi.id
 from mdl_grade_items gi 
 join mdl_modules m on gi.itemmodule = m.name 
 join mdl_course_modules cm on m.id = cm.module and gi.iteminstance = cm.`instance`
where gi.itemname is not null and gi.itemname <> ''
and  m.id=16  and cm.`instance` = 1;
 
 
#conjuntando  las 3 consultas anteriores para obtener la calificaciòn de un alumno para una actividad determinada
#paràmetros recibidos de la programaciòn de evento: g.id(ada grupo_id), cm.instance(actividad_id), m.id(tipo_actividad_id)

SELECT u.id, u.username, u.firstname, u.lastname, u.email,gi.itemname as nombre_actividad, gg.finalgrade,gg.rawgrademin,gg.rawgrademax
/* Para obtener el usuario */
FROM mdl_user u
JOIN mdl_role_assignments ra ON ra.userid = u.id
JOIN mdl_role r ON ra.roleid = r.id
JOIN mdl_context con ON ra.contextid = con.id
JOIN mdl_course c ON c.id = con.instanceid AND con.contextlevel = 50
JOIN mdl_groups_members gm on u.id=gm.userid 
join mdl_groups g on g.id = gm.groupid
/*para obtener las calificaciones */
join mdl_grade_grades gg on u.id = gg.userid
join mdl_grade_items gi on gi.id=gg.itemid
/* Para obtener los elementos evaluables */
join mdl_modules m on gi.itemmodule = m.name 
 join mdl_course_modules cm on m.id = cm.module and gi.iteminstance = cm.`instance`
where gi.itemname is not null and gi.itemname <> ''
and  m.id=1  and cm.`instance` = 1
#Para determinar que el usuario sea alumno y pertenezca al grupo que se requiere
and
r.shortname = 'student' 
AND g.id = 1;
##FIN APARTADO PARA TRATAR DE OBTENER LAS CALIFICACONES DE LOS ALUMNOS DE DETERMINADO (GRUPO, ACTIVIDAD)

select * from mdl_course_modules;
select * from mdl_quiz;
select * from mdl_assign;
select * from mdl_grade_items;
select * from mdl_modules;
select * from mdl_lti;
select * from mdl_grade_grades;
select * from mdl_user

/*########### OBTENER LOS USUARIOS DENTRO DE CADA CURSO Y GRUPO AL QUE PERTENECEN ####### */    
SELECT u.id, u.username, c.fullname, g.name, gm.groupid, ra.roleid, r.shortname 
FROM mdl_user u 
JOIN mdl_role_assignments ra ON ra.userid = u.id 
JOIN mdl_role r ON ra.roleid = r.id 
JOIN mdl_context con ON ra.contextid = con.id 
JOIN mdl_course c ON c.id = con.instanceid AND con.contextlevel = 50 
JOIN mdl_groups_members gm on u.id=gm.userid 
JOIN mdl_groups g on g.id = gm.groupid