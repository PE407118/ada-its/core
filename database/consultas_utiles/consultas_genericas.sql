select * 
from mdl_role r join mdl_role_assignments ra
	on r.id=ra.roleid;


select * from mdl_role_assignments;

select * from mdl_role_allow_assign;



select * from mdl_role_names;

select * from mdl_role_context_levels;
select * from mdl_context;
select * from mdl_user;
select * from mdl_course;

select * from mdl_course_modules;


select * from mdl_question;


select * from mdl_question_answers;

#obtiene las unidades del curso de prueba (Física y su Matemática)

select c.fullname,cs.* from mdl_course_sections cs join mdl_course c
on cs.course=c.id
where c.fullname='Física y su Matemática';


#will get you all the students in a course. mdl_context.instanceid 
#points to different things depending on mdl_context.contextlevel. 50 means course, 
#the other levels can be looked up in the accesslib.php code here 
#http://cvs.moodle.org/moodle/lib/accesslib.php?annotate=1.514#l136, 
#For example, mdl_context.contextlevel 70 is modules, then instanceid points to the mdl_course_modules table.
#https://moodle.org/mod/forum/discuss.php?d=105759
SELECT u.id, u.username FROM
mdl_user u
JOIN mdl_role_assignments ra ON ra.userid = u.id
JOIN mdl_role r ON ra.roleid = r.id
JOIN mdl_context con ON ra.contextid = con.id
JOIN mdl_course c ON c.id = con.instanceid AND con.contextlevel = 50
JOIN mdl_groups_members gm on u.id=gm.userid 
join mdl_groups g on g.id = gm.groupid
WHERE
r.shortname = 'student' AND C.fullname='Física y su Matemática'
AND g.name = 'LAT2';

SELECT * FROM 
mdl_groups_members;
SELECT * FROM mdl_groups;
select * from mdl_grade_items;
select * from mdl_grade_grades where userid = 11;



SELECT gi.id,gg.finalgrade,gg.rawgrademin,gg.rawgrademax
FROM mdl_grade_items gi LEFT JOIN mdl_grade_grades gg 
	ON gi.id=gg.itemid AND gg.userid=11
    WHERE gi.courseid = 4
   and gi.id = 4;


select * from mdl_user;

select * from mdl_groups;
select * from mdl_groups_members;

select * from mdl_lesson_grades;
select * from mdl_assign_grades;
select * from mdl_grade_grades;

select * from mdl_grade_items;

select * from mdl_grade_items gi join mdl_grade_grades gg on gi.id=gg.itemid;
select * from mdl_quiz_grades;




SELECT * FROM
mdl_user u
JOIN mdl_role_assignments ra ON ra.userid = u.id
JOIN mdl_role r ON ra.roleid = r.id
JOIN mdl_context con ON ra.contextid = con.id
JOIN mdl_course c ON c.id = con.instanceid AND con.contextlevel = 50
WHERE
r.shortname = 'student';


#REVISAR PARA LAS CALIFICACIONES
select FROM_UNIXTIME( timemodified, '%Y-%d-%m %h:%i:%s' ) as timemodified, 
mdl_grade_grades.* from mdl_grade_grades;
select * from mdl_quiz_grades;
select * from mdl_grade_items;


select * from mdl_lti;
select * from mdl_assign_overrides;

select * from mdl_groups;

#recupera datos de sobreescritura de actividad por grupo 
select id, assignid, groupid, userid, 
	FROM_UNIXTIME( allowsubmissionsfromdate, '%Y-%d-%m %h:%i:%s' ) as allowsubmissionsfromdate,
	FROM_UNIXTIME( duedate, '%Y-%d-%m %h:%i:%s' ) as duedate,
	FROM_UNIXTIME( cutoffdate, '%Y-%d-%m %h:%i:%s' ) as cutoffdate
from mdl_assign_overrides;


select * from mdl_user;

select * from mdl_user_lastaccess;

select id, username, email,city,country, FROM_UNIXTIME( firstaccess, '%Y-%d-%m %h:%i:%s' ) as firstaccess, 
	FROM_UNIXTIME( lastaccess, '%Y-%d-%m %h:%i:%s' ) as lastaccess, FROM_UNIXTIME( lastlogin, '%Y-%d-%m %h:%i:%s' ) as lastlogin
from mdl_user;

select * from mdl_enrol;
select * from mdl_groups;
select * from mdl_groupings;
select *,  FROM_UNIXTIME( timeadded, '%Y-%d-%m %h:%i:%s' ) from mdl_groups_members;


#Para las actividades
	#Para las actividades de subir
select * from mdl_assign;
select * from mdl_assign_grades;
select * from mdl_assign_overrides;
	#Para los exámenes
select * from mdl_quiz;
select * from mdl_quiz_grades;

select * from mdl_lesson;

select * from mdl_choice;
select * from mdl_choice_answers;

select *,FROM_UNIXTIME(chattime) as chattime from mdl_chat;
select * from mdl_chat_messages;
select *, FROM_UNIXTIME(`timestamp`)as `timestamp` from mdl_chat_messages;

#Calificaciones totales
Select *,FROM_UNIXTIME( timemodified, '%Y-%d-%m %h:%i:%s' ) from mdl_grade_grades;



select * from mdl_grade_items;

select * from mdl_data;

select * from mdl_data_content;
select * from mdl_data_fields;
select * from mdl_data_records;


select * from mdl_user;

select * from mdl_course_modules;
select * from mdl_modules;

select * from mdl_user;


select * from mdl_lesson;


##APARTADO PARA TRATAR DE OBTENER LAS CALIFICACONES DE LOS ALUMNOS DE DETERMINADO (GRUPO, ACTIVIDAD)

#obtenidendo los usuarios pertenecientes a un grupo especìfico (el grupo solo tiene un curso)
SELECT u.id, u.username FROM
mdl_user u
JOIN mdl_role_assignments ra ON ra.userid = u.id
JOIN mdl_role r ON ra.roleid = r.id
JOIN mdl_context con ON ra.contextid = con.id
JOIN mdl_course c ON c.id = con.instanceid AND con.contextlevel = 50
JOIN mdl_groups_members gm on u.id=gm.userid 
join mdl_groups g on g.id = gm.groupid
WHERE
r.shortname = 'student' 
AND g.id = 1;


#obtenidendo las calificaciones de un usuario en un curso
SELECT gi.id,gg.finalgrade,gg.rawgrademin,gg.rawgrademax
FROM mdl_grade_items gi LEFT JOIN mdl_grade_grades gg 
	ON gi.id=gg.itemid AND gg.userid=11
    WHERE gi.courseid = 4;
   
 #Obteniendo gi con el criterio de bùsqueda de actividad
 select gi.id
 from mdl_grade_items gi 
 join mdl_modules m on gi.itemmodule = m.name 
 join mdl_course_modules cm on m.id = cm.module and gi.iteminstance = cm.`instance`
where gi.itemname is not null and gi.itemname <> ''
and  m.id=16  and cm.`instance` = 1;
 
 
#conjuntando  las 3 consultas anteriores para obtener la calificaciòn de un alumno para una actividad determinada
#paràmetros recibidos de la programaciòn de evento: g.id(ada grupo_id), cm.instance(actividad_id), m.id(tipo_actividad_id)

SELECT u.id, u.username, u.firstname, u.lastname , u.email,gi.itemname as nombre_actividad, gg.finalgrade,gg.rawgrademin,gg.rawgrademax
/* Para obtener el usuario */
FROM mdl_user u
JOIN mdl_role_assignments ra ON ra.userid = u.id
JOIN mdl_role r ON ra.roleid = r.id
JOIN mdl_context con ON ra.contextid = con.id
JOIN mdl_course c ON c.id = con.instanceid AND con.contextlevel = 50
JOIN mdl_groups_members gm on u.id=gm.userid 
join mdl_groups g on g.id = gm.groupid
/*para obtener las calificaciones */
join mdl_grade_grades gg on u.id = gg.userid
join mdl_grade_items gi on gi.id=gg.itemid
/* Para obtener los elementos evaluables */
join mdl_modules m on gi.itemmodule = m.name 
 join mdl_course_modules cm on m.id = cm.module and gi.iteminstance = cm.`instance`
where gi.itemname is not null and gi.itemname <> ''
and  m.id=1  and cm.`instance` = 1
#Para determinar que el usuario sea alumno y pertenezca al grupo que se requiere
and
r.shortname = 'student' 
AND g.id = 1;
##FIN APARTADO PARA TRATAR DE OBTENER LAS CALIFICACONES DE LOS ALUMNOS DE DETERMINADO (GRUPO, ACTIVIDAD)

select * from mdl_course_modules;
select * from mdl_quiz;
select * from mdl_assign;
select * from mdl_grade_items;
select * from mdl_modules;
select * from mdl_lti;
select * from mdl_grade_grades;
select * from mdl_user;
select * from mdl_assign;


SELECT u.id, u.username, u.firstname, u.lastname, u.email,gi.itemname as nombre_actividad, gg.finalgrade,gg.rawgrademin,gg.rawgrademax
/* Para obtener el usuario */
FROM mdl_user u
JOIN mdl_role_assignments ra ON ra.userid = u.id
JOIN mdl_role r ON ra.roleid = r.id
JOIN mdl_context con ON ra.contextid = con.id
JOIN mdl_course c ON c.id = con.instanceid AND con.contextlevel = 50
JOIN mdl_groups_members gm on u.id=gm.userid 
join mdl_groups g on g.id = gm.groupid
/*para obtener las calificaciones */
join mdl_grade_grades gg on u.id = gg.userid
join mdl_grade_items gi on gi.id=gg.itemid
/* Para obtener los elementos evaluables */
join mdl_modules m on gi.itemmodule = m.name 
 join mdl_course_modules cm on m.id = cm.module and gi.iteminstance = cm.`instance`
where gi.itemname is not null and gi.itemname <> ''
and  m.id=1  and cm.`instance` = 3
#Para determinar que el usuario sea alumno y pertenezca al grupo que se requiere
and
r.shortname = 'student' 
AND g.id = 1;
update mdl_grade_grades set finalgrade=8 where id = 11;
select * from mdl_grade_grades;

SELECT distinct gg.rawgrademax
/* Para obtener el usuario */
FROM mdl_user u
JOIN mdl_role_assignments ra ON ra.userid = u.id
JOIN mdl_role r ON ra.roleid = r.id
JOIN mdl_context con ON ra.contextid = con.id
JOIN mdl_course c ON c.id = con.instanceid AND con.contextlevel = 50
JOIN mdl_groups_members gm on u.id=gm.userid 
join mdl_groups g on g.id = gm.groupid
/*para obtener las calificaciones */
join mdl_grade_grades gg on u.id = gg.userid
join mdl_grade_items gi on gi.id=gg.itemid
/* Para obtener los elementos evaluables */
join mdl_modules m on gi.itemmodule = m.name 
 join mdl_course_modules cm on m.id = cm.module and gi.iteminstance = cm.`instance`
where gi.itemname is not null and gi.itemname <> ''
and  m.id=1  and cm.`instance` = 3
#Para determinar que el usuario sea alumno y pertenezca al grupo que se requiere
and
r.shortname = 'student' 
AND g.id = 1;

select * from mdl_assign;


select * from mdl_messages;

select * from mdl_message_conversations;
select * from mdl_message_conversations;

select * from  mdl_message_conversation_members; 
SELECT UNIX_TIMESTAMP();
insert into mdl_message_conversations (type,enabled,timecreated) values(1,1,UNIX_TIMESTAMP());
insert into mdl_message_conversation_members (conversationid, userid,timecreated) values (5,14,UNIX_TIMESTAMP());
insert into mdl_message_conversation_members (conversationid, userid,timecreated) values (5,11,UNIX_TIMESTAMP());


INSERT INTO mdl_messages (useridfrom,conversationid,fullmessage,fullmessageformat,smallmessage,timecreated) 
values (14,5,'Este es mi mensaje desde la BD, hola Cat',0,'Este es mi mensaje desde la BD, hola Cat',UNIX_TIMESTAMP());


INSERT INTO mdl_messages (useridfrom,conversationid,fullmessage,fullmessageformat,smallmessage,timecreated) 
values (14,5,'otro mensaje',0,'otro mensaje',UNIX_TIMESTAMP());

INSERT INTO mdl_messages (useridfrom,conversationid,fullmessage,fullmessageformat,smallmessage,timecreated) 
values (14,5,'<p>¡Puedes hacerlo mejor! Te sugerimos el siguiente material para reforzar tu aprendizaje.</p><p><a href="u2" target="_blank" class="uapa-notificacion">uapa2</a></p>',0,'<p>¡Puedes hacerlo mejor! Te sugerimos el siguiente material para reforzar tu aprendizaje.</p><p><a href="u2" target="_blank" class="uapa-notificacion">uapa2</a></p>',UNIX_TIMESTAMP());

select conversationid
from mdl_message_conversation_members
where userid in (14,11)
group by conversationid
having count(*)=2;

delete from mdl_message_conversations where id=5;
delete from mdl_message_conversations where id>4;


select * from mdl_role;

select *, FROM_UNIXTIME(timecreated) from mdl_assign_submission;



#obtenidendo los usuarios pertenecientes a un grupo especìfico (el grupo solo tiene un curso)
SELECT u.id, u.username FROM
mdl_user u
JOIN mdl_role_assignments ra ON ra.userid = u.id
JOIN mdl_role r ON ra.roleid = r.id
JOIN mdl_context con ON ra.contextid = con.id
JOIN mdl_course c ON c.id = con.instanceid AND con.contextlevel = 50
JOIN mdl_groups_members gm on u.id=gm.userid 
join mdl_groups g on g.id = gm.groupid
WHERE
r.shortname = 'student' 
AND g.id = 1;



#consulta que obtiene los alumnos que no han participado en una actividad
SELECT u.id, u.username, u.firstname, u.lastname, u.email
FROM mdl_user u
JOIN mdl_role_assignments ra ON ra.userid = u.id
JOIN mdl_role r ON ra.roleid = r.id
JOIN mdl_context con ON ra.contextid = con.id
JOIN mdl_course c ON c.id = con.instanceid AND con.contextlevel = 50
JOIN mdl_groups_members gm on u.id=gm.userid 
join mdl_groups g on g.id = gm.groupid
where g.id =2
and u.id not in (
	select distinct userid
	from mdl_quiz_attempts
	where state = 'finished'
	and quiz = 1
	
);

#Obteniendo los alumnos que subieron un archivo a las actividades "ASSIGNMENT" (lo toma de la tabla assign)
select userid 
	from mdl_assign_submission as ass
	where ass.status = 'submitted'
	and ASSIGNMENT=4;

#Obteniendo los alumnos que participaron en un foro
select distinct fp.userid
	from mdl_forum_posts fp join mdl_forum_discussions fd
		on fp.discussion = fd.id
	where groupid=2
	and forum =1;

#Obteniendo los alumnos que participaron en un chat
select distinct userid
from mdl_chat_messages
where chatid=1
and groupid =1;

#Obteniendo los alumnos que ya respondierion la actividad tipo quiz
select distinct userid
from mdl_quiz_attempts
where state = 'finished'
and quiz = 1;



select * from mdl_quiz;
select * from mdl_quiz_grades;
select * from mdl_quiz_attempts;
select * from mdl_grade_items;
select * from mdl_quiz

select * from mdl_chat;

select * from mdl_chat_messages;





select * from mdl_forum;
select * from mdl_forum_posts;
select * from mdl_forum_discussions;

delete from mdl_forum_posts where userid=12;

select * from mdl_modules;




select * from mdl_assignment;










select * from mdl_assignment_submissions;

select * from mdl_Groups;

select *, FROM_UNIXTIME(timecreated) from mdl_assign_submission ;

select * from mdl_assign;

SELECT u.id, u.username FROM
mdl_user u
JOIN mdl_role_assignments ra ON ra.userid = u.id
JOIN mdl_role r ON ra.roleid = r.id
JOIN mdl_context con ON ra.contextid = con.id
JOIN mdl_course c ON c.id = con.instanceid AND con.contextlevel = 50
JOIN mdl_groups_members gm on u.id=gm.userid 
join mdl_groups g on g.id = gm.groupid
WHERE
r.shortname = 'student' 
AND g.id = 2;


select * from mdl_assign where id=4;

select * from mdl_assign_user_mapping;
