#consulta que obtiene los alumnos que no han participado en una actividad
SELECT u.id, u.username, u.firstname, u.lastname, u.email
FROM mdl_user u
JOIN mdl_role_assignments ra ON ra.userid = u.id
JOIN mdl_role r ON ra.roleid = r.id
JOIN mdl_context con ON ra.contextid = con.id
JOIN mdl_course c ON c.id = con.instanceid AND con.contextlevel = 50
JOIN mdl_groups_members gm on u.id=gm.userid 
join mdl_groups g on g.id = gm.groupid
where g.id =2
and u.id not in (
	select distinct userid
	from mdl_quiz_attempts
	where state = 'finished'
	and quiz = 1
	
);

#Obteniendo los alumnos que subieron un archivo a las actividades "ASSIGNMENT" (lo toma de la tabla assign)
select userid 
	from mdl_assign_submission as ass
	where ass.status = 'submitted'
	and ASSIGNMENT=4;

#Obteniendo los alumnos que participaron en un foro
select distinct fp.userid
	from mdl_forum_posts fp join mdl_forum_discussions fd
		on fp.discussion = fd.id
	where groupid=2
	and forum =1;

#Obteniendo los alumnos que participaron en un chat
select distinct userid
from mdl_chat_messages
where chatid=1
and groupid =1;

#Obteniendo los alumnos que ya respondierion la actividad tipo quiz
select distinct userid
from mdl_quiz_attempts
where state = 'finished'
and quiz = 1;


